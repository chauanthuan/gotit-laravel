<?php

namespace App\Helpers;

class Currency{

    public static function formatMoneyWithSup($price){
        if(empty($price)) return '0';
        return number_format($price, 0, '', '.').'<sup>đ</sup>';
    }
    public static function formatMoney($price){
        if(empty($price)) return '0';
        return number_format($price, 0, '', '.');
    }
}