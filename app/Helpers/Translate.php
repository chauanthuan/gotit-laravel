<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class Translate{

    public static function transObj($object, $index, $lang = null){
		$ss_lang = Session::get('laravel_language', 'vi');

        if(!$lang){
            $lang = Cookie::get('laravel_language', $ss_lang);
        }

        if(!in_array($lang, ['vi', 'en', 'ko'])){
            $lang = 'vi';
        }

        if($lang == "ko"){
        	$key = $index . "_en";
        }else{
            $key = $index . "_" . $lang;
    	}

        return $object->$key;
    }

    public static function trans_date($date){
		$ss_lang = Session::get('laravel_language', 'vi');
        $lang = Cookie::get('laravel_language', $ss_lang);

        if(empty($lang) || $lang == 'vi'){
            $date = strftime("%d %B, %Y, %I:%M %p", strtotime($date));

            $patterns = array();
            $patterns[0] = '/January/';
            $patterns[1] = '/February/';
            $patterns[2] = '/March/';
            $patterns[3] = '/April/';
            $patterns[4] = '/May/';
            $patterns[5] = '/June/';
            $patterns[6] = '/July/';
            $patterns[7] = '/August/';
            $patterns[8] = '/September/';
            $patterns[9] = '/October/';
            $patterns[10] = '/November/';
            $patterns[11] = '/December/';

            $replacements = array();
            $replacements[11] = 'Tháng Một';
            $replacements[10] = 'Tháng Hai';
            $replacements[9] = 'Tháng Ba';
            $replacements[8] = 'Tháng Tư';
            $replacements[7] = 'Tháng Năm';
            $replacements[6] = 'Tháng Sáu';
            $replacements[5] = 'Tháng Bảy';
            $replacements[4] = 'Tháng Tám';
            $replacements[3] = 'Tháng Chín';
            $replacements[2] = 'Tháng Mười';
            $replacements[1] = 'Tháng Mười Một';
            $replacements[0] = 'Tháng Mười Hai';

            $date = preg_replace($patterns, $replacements, $date);
        }else{
            $date = strftime("%B %d, %Y, %I:%M %p", strtotime($date));
        }

        return $date;
    }
}