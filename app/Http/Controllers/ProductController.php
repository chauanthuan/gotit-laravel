<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class ProductController extends Controller
{
    public function detail(Request $request){
    	$product_id = 896;

    	$product = Product::findOrFail($product_id);
    	return view('product.detail', compact('product'));
    }
}
