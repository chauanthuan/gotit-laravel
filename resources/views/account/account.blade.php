@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/account.css') !!}">
@endsection
@section('content')
<section class="accountsetting-section">
	<div class="container">
	    <div class="row parent" id="parent" style="min-height: 170px;">
			<div class="col-md-3 left child">
				@include('account.menu-left')
			</div>
				<div class="col-md-9 right child">
				<div class="wrap_gift">
					<div class="box_top">
						<h2 class="">Account Setting</h2>
					</div>
          <div class="box_content">
            <div class="box_item">
              
              <h4>Personal Informations</h4>

              <div class="personal_info">
              	<div class="row">
              		<div class="col-md-6">
              			<p class="title">Fullname</p>
              			<div class="group_two_col">
              				<div class="form-group form-md-floating-label left">
              					<input id="firstname" name="firstname" type="text" class="form-control edited" value="Nhật">
              					<label for="firstname">Firstname </label>
              				</div>
              				
              				<div class="form-group form-md-floating-label right">
              					<input id="lastname" name="lastname" type="tel" class="form-control edited" value="Nguyễn">
              					<label for="lastname">Lastname</label>
              				</div>
              				
              			</div>
              		</div>
              		<div class="col-md-6 birthday">
              			<p class="title">Birthday</p>
              			<div class="group_three_col">
              				<div class="month">
              					<p>Month</p>
                        <select class="selectpicker select_month form-control" title="Month" data-size="5" >
                          <option>January</option>
                          <option>February</option>
                          <option>March</option>
                          <option>April</option>
                          <option>March</option>
                          <option>May</option>
                          <option>June</option>
                          <option>July</option>
                          <option>August</option>
                          <option>September</option>
                          <option>October</option>
                          <option>November</option>
                        </select>
                      </div>
              				<div class="day">
              					<p>Day</p>
              		     <select class="selectpicker select_month form-control" title="Day" data-size="5" >
                          @for ($i = 1; $i <= 31; $i++)
                              <option>{{$i}}</option>
                          @endfor
                          </select>
              				</div>
              				<div class="year">
              					<p>Year</p>
                        <select class="selectpicker select_month form-control" title="Year" data-size="5" >
                          @for ($i = 2018; $i >= 1938; $i--)
                              <option>{{$i}}</option>
                          @endfor
                          </select>
              				</div>
              			</div>
              		</div>
              	</div>            	
              	<div class="row">
              		<div class="Gender">
              		<p class="title" for="gender">Gender</p>
              		<div class="wrap-pr-type">
              			<ul>
              				<li>
              					<input class="styled ng-untouched ng-pristine ng-valid" id="M" name="gender" type="radio" value="M" checked>
              					<label class="" for="M">Male</label>
              				</li>
              				<li>
              					<input class="styled ng-untouched ng-pristine ng-valid" id="F" name="gender" type="radio" value="F">
              					<label class="" for="F">Female</label>
              				</li>
              			</ul>
              		</div>		
              	</div>
              </div>
              </div>
            </div> 
            <div class="box_item box_contact">
            	<h4>Contact Details</h4>
            	<div class="contact_details">
            		<div class="row">
                  <div class="col-md-12">
                
                  </div>
            			<div class="col-md-6">
            				<p class="title" for="Inputpassword3">Email</p>
            				<div class="form-group form-md-floating-label left">
            					<input autocomplete="off" id="email" name="email" readonly="" type="text" class="form-control edited" value="nhat@gmail.com">
            					<label for="email">Your email</label>
            				</div>
            			</div>
            			<div class="col-md-6">
            				<p class="title" for="Inputpassword3">Mobile</p>
            				<div class="form-group form-md-floating-label right">
            					<input autocomplete="off" id="phonenumber" name="phonenumber" pattern="^(\+84|0)(9|8|1[2689])\d{8}$" type="text" class="form-control ng-untouched ng-pristine ng-valid">
            					<label for="phonenumber">Your phone number</label>
            				</div>     			
            			</div>
            		</div>           		
            	</div>
            </div>

          </div>

          <div class="box_change">
          	
          	<div class="pull-right change-btn">
          		<a>Save Change</a>
          	</div>

          </div>
				</div>
			</div>
	<div class="left-fixed-wrap" ></div>
</section>

@endsection
@section('scripts')
<script>
  $(document).ready(function() {
     $('#account-detail p.account a').addClass('active');
    $('.form-control').change(function(){
      var $this = $(this);
      if($this.val())
        $this.addClass('edited')
      else
        $this.removeClass('edited')
    });
    var w_width = $(window).width();
    var content_width = $('.accountsetting-section .parent').width();
    var left_fixed = (w_width - content_width)/2;
    $('.left-fixed-wrap').css({'width':left_fixed+'px'});
  });
  // $('.control_month').click(function() {

 //    $('.btn_month').hide();
 //    $('div.select_month').show();
 //    $('p.test01').css('color', 'red');;
 //    $('div.select_month').css('background', 'red');
  // });
</script>
@endsection
