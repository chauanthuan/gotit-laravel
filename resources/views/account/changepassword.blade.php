@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/changepassword.css') !!}">
@endsection
@section('content')

<section class="changepassword-section">
	<div class="container">
	    <div class="row parent" id="parent" style="min-height: 170px;">
			<div class="col-md-3 left child">
        @include('account.menu-left')
			</div>
			<div class="col-md-9 right child">
				<div class="wrap_gift">
					<div class="box_top">
              <h2 class="">Change Password</h2>
            
          </div>
            <div class="box_content password">
            <div class="box_item">
              <h4>Password Infomations</h4>
                <div class="row">
                  <form novalidate="" class="ng-untouched ng-pristine ng-invalid">
                  
                    <div class="col-md-12">
                    </div>
                    <div class="col-md-4">
                      <p class="title" for="Inputpassword1">Current</p>
                     <div class="form-group form-md-floating-label">
                      
                      <input autocomplete="off" id="Inputpassword1" name="password" required="" type="password" class="form-control ng-untouched ng-pristine ng-invalid">
                      <label for="password">Password</label>
         
                      </div>
                    </div>
                    <div class="col-md-4">
                      <p class="title" for="Inputpassword2">New</p>
                      <div class="form-group form-md-floating-label">
                      
                      <input autocomplete="off" id="newpassword" name="newpassword" required="" type="password" class="form-control ng-untouched ng-pristine ng-invalid">
                      <label for="newpassword"> New password</label>
                      
                      </div>
                    </div>
                    <div class="col-md-4">
                      <p class="title" for="Inputpassword3">Confirm</p>
                      <div class="form-group form-md-floating-label">
                      
                      <input autocomplete="off" id="confirmpassword" name="confirmpassword" required="" type="password" class="form-control ng-untouched ng-pristine ng-invalid">
                      <label for="confirmpassword">Confirm password</label>
                      
                      </div>
                    </div>
                  </form>
                </div>
             </div> 
          </div>

          <div class="box_change">
              <button class="btn change-btn" type="submit" disabled="">Change</button>
          </div>
          
				</div>
			</div>
			<div class="left-fixed-wrap" style="width: 89.5px;"></div>
	  	</div>
	</div>
</section>
@endsection

@section('scripts')
<script>
  $(document).ready(function() {
    $('#account-detail p.changepassword a').addClass('active');

    $('.form-control').change(function(){
      var $this = $(this);
      if($this.val())
        $this.addClass('edited')
      else
        $this.removeClass('edited')
    });

    var w_width = $(window).width();
    var content_width = $('.accountsetting-section .parent').width();
    var left_fixed = (w_width - content_width)/2;
    $('.left-fixed-wrap').css({'width':left_fixed+'px'});
  });
  // var w_height = $(window).height();
  //   console.log(w_height);
  //   var height_header  = $('.showMenu').height();
  //   console.log(height_header);
  //   var height_footer  = $('footer').height();
  //   console.log(height_footer);

  //   var height_content = w_height - (height_header+height_footer);
  //   console.log(height_content);
  //   $('#parent').css({'height':height_content+'px'});
</script>
@endsection