@extends('layouts.master')
@section('styles')
  <link rel="stylesheet" href="{!! asset('css/checkout.css') !!}">
@endsection

@section('content')
<section class="product-section checkout-section" id="pg_checkout">
   <div class="container">
      <div id="parent" class="row parent " style="min-height: 602px;">
         <div class="col-md-9 left child">
            <div class="breadcrumb">
               <p class="step-1 active"><span></span>Choose eGift</p>
               <p class="step-2 active"><span></span>Enter receiver detail and your message</p>
               <p class="step-3 "><span></span>Payment</p>
            </div>
            <div class="wrapper-bx write_message active">
               <div class="box_top tabbable-line">
                  <h2 class="">Write your message <a href="javscript:void(0)" class="btn btn-link edit-message-link en">Edit</a></h2>
                  <p class="notice">Please choose the channel that you want to send and fill the information</p>
               </div>
               <div class="box-content">
                  <div class="col-md-3">
                     <div class="wrapper-dropdown active" id="tab_all_chanel" tabindex="1">
                        <span>Choose channel<small class="sms_cn">sms</small></span>
                        <ul class="channel tabbable dropdown">
                           <li data-toggle="tab" href="#tab_sms" class="active">
                              <p class="sms_cn">
                                 <label class="" for="sms"><span></span>SMS</label>
                              </p>
                           </li>
                           <li data-toggle="tab" href="#tab_email" class="">
                              <p class="email_cn">
                                 <label class="" for="email"><span></span>Email</label>
                              </p>
                           </li>
                           <li data-toggle="tab" href="#tab_messenger" class="hide_onmobile ">
                              <p class="messenger_cn">
                                 <label class="" for="messenger"><span></span>Messenger</label>
                              </p>
                           </li>
                           <li data-toggle="tab" href="#tab_viber" class="hide_ondesktop ">
                              <p class="viber_cn">
                                 <label class="" for="viber"><span></span>Viber</label>
                              </p>
                           </li>
                           <li data-toggle="tab" href="#tab_whatsapp" class="hide_ondesktop ">
                              <p class="whatsapp_cn">
                                 <label class="" for="whatsapp"><span></span>Whatsapp</label>
                              </p>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-9">
                     <div class="tab-content">
                        <div id="tab_sms" class="tab-pane active">
                           <div class="col-md-12">
                              <div class="row">
                                 <div class="receiver_info">
                                    <div class="col-md-12">
                                       <p>Receiver Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       
                                    </div>
                                    <div class="col-md-12">
                                       <div class="group_two_col">
                                          <div class="form-group form-md-floating-label left">
                                             <input id="form_control_name_sms" placeholder="Nguyen Van A" type="email" class="form-control edited ng-untouched ng-pristine ng-valid">
                                             <label for="form_control_name_sms">Receiver's name</label>
                                          </div>
                                          <div class="form-group form-md-floating-label right">
                                             <input id="form_control_phone" pattern="^(\+84|0)(9|8|1[2689])\d{8}$" placeholder="09xxxxxxxx" type="tel" class="form-control edited ng-untouched ng-pristine ng-valid">
                                             <label class="phone_label" for="form_control_phone">Receiver’s phone number
                                             <span class="icon-information">
                                             <small class="note_security_code">By format: +84xxxxxxxx or 0xxxxxxxxx</small>
                                             </span>
                                             </label>
                                          </div>
                                       </div>
                                       <div class="form-group form-md-floating-label hide">
                                          <textarea id="form_control_area" rows="3" type="text" class="form-control ng-untouched ng-pristine ng-valid"></textarea>
                                          <label for="form_control_area">Your message</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row hide">
                                 <div class="sender_info">
                                    <div class="col-md-12">
                                       <p class="title">Sender Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group form-md-floating-label">
                                          <input id="customer_name_sms" type="text" class="sender_name form-control edited ng-untouched ng-pristine ng-valid">
                                          <label for="customer_name_sms">Your name</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="tab_email" class="tab-pane ">
                           <div class="col-md-12">
                              <div class="row">
                                 <div class="receiver_info">
                                    <div class="col-md-12">
                                       <p>Receiver Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       
                                    </div>
                                    <div class="col-md-12">
                                       <div class="group_two_col">
                                          <div class="form-group form-md-floating-label left">
                                             <input id="receiver_name_em" placeholder="Nguyen Van A" type="email" class="form-control edited ng-untouched ng-pristine ng-valid">
                                             <label for="receiver_name_em">Receiver's name</label>
                                          </div>
                                          <div class="form-group form-md-floating-label right">
                                             <input id="receiver_email_em" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" placeholder="example@example.com" type="email" class="form-control edited ng-untouched ng-pristine ng-valid">
                                             <label for="receiver_email_em">Receiver's email</label>
                                          </div>
                                       </div>
                                       <div class="form-group form-md-floating-label">
                                          <textarea id="form_control_area" rows="3" type="text" class="form-control ng-untouched ng-pristine ng-valid"></textarea>
                                          <label for="form_control_area">Your message</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="sender_info">
                                    <div class="col-md-12">
                                       <p class="title">Sender Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       
                                    </div>
                                    <div class="col-md-12">
                                       <div class="group_two_col">
                                          <div class="form-group form-md-floating-label left">
                                             <input id="customer_name_em" placeholder="Nguyen Van A" type="text" class="sender_name form-control edited ng-untouched ng-pristine ng-valid">
                                             <label for="customer_name_em">Your name</label>
                                          </div>
                                          <div class="form-group form-md-floating-label right">
                                             <input id="customer_email_em" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" placeholder="example@example.com" type="email" class="sender_email form-control edited ng-untouched ng-pristine ng-valid">
                                             <label for="customer_email_em">Your email</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="tab_messenger" class="tab-pane ">
                           <div class="col-md-12">
                              <div class="row">
                                 <div class="receiver_info">
                                    <div class="col-md-12 noitice">
                                       <ol class="">
                                          <li>Enter Friend’s name and your Message</li>
                                          <li>Make Payment</li>
                                          <li>Send your gift via Messenger</li>
                                       </ol>
                                    </div>
                                    <div class="col-md-12">
                                       <p>Receiver Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group form-md-floating-label">
                                          <input id="receiver_name_msg" placeholder="Nguyen Van A" type="email" class="form-control edited ng-untouched ng-pristine ng-valid">
                                          <label for="receiver_name_msg">Receiver's name</label>
                                       </div>
                                       <div class="form-group form-md-floating-label">
                                          <textarea id="form_control_area" rows="3" type="text" class="form-control ng-untouched ng-pristine ng-valid"></textarea>
                                          <label for="form_control_area">Your message</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row hide">
                                 <div class="sender_info">
                                    <div class="col-md-12">
                                       <p class="title">Sender Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group form-md-floating-label">
                                          <input id="customer_name_msg" placeholder="Nguyen Van A" type="text" class="sender_name form-control edited ng-untouched ng-pristine ng-valid">
                                          <label for="customer_name_msg">Your name</label>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group form-md-floating-label">
                                          <input id="customer_email_msg" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" placeholder="example@example.com" type="email" class="sender_email form-control edited ng-untouched ng-pristine ng-valid">
                                          <label for="customer_email_msg">Your email</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="tab_viber" class="tab-pane hide_ondesktop  ">
                           <div class="col-md-12">
                              <div class="row">
                                 <div class="noitice">
                                    <ol class="">
                                       <li>Enter Your Name and Receiver Informations</li>
                                       <li>Open Viber App</li>
                                       <li>Choose friend and send your gift link</li>
                                    </ol>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="receiver_info">
                                    <div class="col-md-12">
                                       <p>Receiver Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group form-md-floating-label">
                                          <input id="receiver_name_viber" type="email" class="form-control ng-untouched ng-pristine ng-valid">
                                          <label for="receiver_name_viber">Receiver's name</label>
                                       </div>
                                       <div class="form-group form-md-floating-label">
                                          <textarea id="form_control_area" rows="3" type="text" class="form-control ng-untouched ng-pristine ng-valid"></textarea>
                                          <label for="form_control_area">Your message</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="sender_info">
                                    <div class="col-md-12">
                                       <p class="title">Sender Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group form-md-floating-label">
                                          <input id="customer_name_viber" type="text" class="form-control sender_name edited ng-untouched ng-pristine ng-valid">
                                          <label for="customer_name_viber">Your name</label>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group form-md-floating-label">
                                          <input id="sender_email_viber" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" type="email" class="form-control sender_email edited ng-untouched ng-pristine ng-valid">
                                          <label for="sender_email_viber">Your email</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="tab_whatsapp" class="tab-pane hide_ondesktop  ">
                           <div class="col-md-12">
                              <div class="row ">
                                 <div class="noitice">
                                    <ol class="">
                                       <li>Enter Your Name and Receiver Informations</li>
                                       <li>Open Whatsapp App</li>
                                       <li>Choose friend and send your gift link</li>
                                    </ol>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="receiver_info">
                                    <div class="col-md-12">
                                       <p>Receiver Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group form-md-floating-label">
                                          <input id="receiver_name_whatsapp" type="text" class="form-control ng-untouched ng-pristine ng-valid">
                                          <label for="receiver_name_whatsapp">Receiver's name</label>
                                       </div>
                                       <div class="form-group form-md-floating-label">
                                          <textarea id="form_control_area" rows="3" type="text" class="form-control ng-untouched ng-pristine ng-valid"></textarea>
                                          <label for="form_control_area">Your message</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="sender_info">
                                    <div class="col-md-12">
                                       <p class="title">Sender Information</p>
                                    </div>
                                    <div class="col-md-12">
                                       
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group form-md-floating-label">
                                          <input id="customer_name_whatsapp" type="text" class="form-control sender_name edited ng-untouched ng-pristine ng-valid">
                                          <label for="customer_name_whatsapp">Your name</label>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group form-md-floating-label">
                                          <input id="sender_email_whatsapp" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" type="email" class="form-control sender_email edited ng-untouched ng-pristine ng-valid">
                                          <label for="sender_email_whatsapp">Your email</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <a href="javascript:void(0)" class="btn btn-next next-write-message disabled">Next</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="wrapper-bx payment_method  ">
               <div class="box_top">
                  <h2 class="">Choose payment method </h2>
                  <p class="notice">Please choose the payment method below</p>
               </div>
               <div class="box-content">
                  <div class="col-md-3">
                     <div class="wrapper-dropdown" id="tab_all_payment" tabindex="1">
                        <span>
                           Choose payment method
                           <small class="credit_cn">
                           Credit Card</small>  
                        </span>
                        <ul class="channel payment-channel tabbable dropdown">
                           <li data-toggle="tab" href="#tab_credit" class="active">
                              <p class="credit_cn">
                                 <label class="" for="credit_cn"><span><img src="/img/pay_creditcard.png"></span>Credit Card</label>
                              </p>
                           </li>
                           <li data-toggle="tab" href="#tab_atm" class="">
                              <p class="credit_cn">
                                 <label class="" for="credit_cn"><span><img src="/img/atm-card.png"></span>ATM</label>
                              </p>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-9">
                     <div class="tab-content">
                        <div id="tab_credit" class="tab-pane active">
                           <div class="col-md-12">
                              <div class="row creditcard_info">
                                 <div class="col-md-12 noitice">
                                    <p>Secure credit payment. All transactions are secured and encrypted(using the payment gate way Cybers System of Sacombank)</p>
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group form-md-floating-label">
                                       <input autocomplete="cc-number" id="card_number" required="" type="text" class="form-control ng-untouched ng-pristine ng-invalid">
                                       <label for="card_number">Card number</label>
                                    </div>
                                    <div class="form-group form-md-floating-label">
                                       <input autocomplete="cc-name" id="name_card" required="" type="text" class="form-control ng-untouched ng-pristine ng-invalid">
                                       <label for="name_card">Name on card</label>
                                    </div>
                                    <div class="group_two_col">
                                       <div class="form-group form-md-floating-label left">
                                          <input autocomplete="cc-exp" class="form-control edited ng-untouched ng-pristine ng-invalid" id="expired_date" placeholder="MM/YY" required="" type="text">
                                          <label for="expired_date">Expired date</label>
                                       </div>
                                       <div class="form-group form-md-floating-label right">
                                          <input autocomplete="cc-csc" id="security_code" required="" type="text" class="form-control ng-untouched ng-pristine ng-invalid">
                                          <label class="security_code" for="security_code">Security code
                                          <span class="icon-information">
                                          <small class="note_security_code">The last 3 digits displayed on the back of your card</small>
                                          </span>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="tab_atm" class="tab-pane ">
                           <div class="col-md-12">
                              <div class="row creditcard_info">
                                 <div class="col-md-12">
                                    <div class="list-bank">
                                       
                                       <div class="bank_item item1">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/vietcombank.png" alt="Vietcombank">
                                          </a>
                                       </div>
                                       <div class="bank_item item2">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/techcombank.png" alt="Techcombank">
                                          </a>
                                       </div>
                                       <div class="bank_item item3">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/vibbank.png" alt="VIB  Bank">
                                          </a>
                                       </div>
                                       <div class="bank_item item4">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/abbank.png" alt="ABBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item5">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/sacombank.png" alt="Sacombank">
                                          </a>
                                       </div>
                                       <div class="bank_item item6">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/maritimebank.png" alt="Maritime  Bank">
                                          </a>
                                       </div>
                                       <div class="bank_item item7">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/navibank.png" alt="Navibank">
                                          </a>
                                       </div>
                                       <div class="bank_item item8">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/viettinbank.png" alt="Vietinbank">
                                          </a>
                                       </div>
                                       <div class="bank_item item9">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/dongabank.png" alt="DongABank">
                                          </a>
                                       </div>
                                       <div class="bank_item item10">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/hdbank.png" alt="HDBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item11">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/vietabank.png" alt="VietABank">
                                          </a>
                                       </div>
                                       <div class="bank_item item12">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/vpbank.png" alt="VPBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item13">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/acb.png" alt="ACB">
                                          </a>
                                       </div>
                                       <div class="bank_item item14">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/mbbank.png" alt="MBBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item15">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/gpbank.png" alt="GPBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item16">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/eximbank.png" alt="Eximbank">
                                          </a>
                                       </div>
                                       <div class="bank_item item17">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/oceanbank.png" alt="OceanBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item18">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/bacabank.png" alt="BacABank">
                                          </a>
                                       </div>
                                       <div class="bank_item item19">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/ocb.png" alt="OricomBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item20">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/tpbank.png" alt="TPBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item21">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/lienvietpostbank.png" alt="LienVietPostBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item22">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/bidv.png" alt="BIDV">
                                          </a>
                                       </div>
                                       <div class="bank_item item23">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/agribank.png" alt="AgriBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item24">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/baovietbank.png" alt="BaoVietBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item25">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/shb.png" alt="SHB">
                                          </a>
                                       </div>
                                       <div class="bank_item item26">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/kienlongbank.png" alt="KienLongBank">
                                          </a>
                                       </div>
                                       <div class="bank_item item27">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/scb.png" alt="SCB">
                                          </a>
                                       </div>
                                       <div class="bank_item item28">
                                          <a href="javascript:void(0)">
                                          <img src="/img/bank/seabank.png" alt="Seabank">
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <a href="javascript:void(0)" class="btn btn-next next-payment">Pay 50,000 VND</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="wrapper-bx finish_box ">
               <div class="box_top">
                  <h2 class="">Thank you</h2>
                  <p>Thanks for using Got It.</p>
                  <p>
                     Please select "<a href="/gift-sent">Send Again</a>" to re-send incompletely sending gift.
                  </p>
               </div>
               <div class="box-content">
                  <div class="col-md-12">
                     <div class="payment_sent_by_method">
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="payment_result">
                        <div class="item">
                           <div class="left">
                              <p>From</p>
                           </div>
                           <div class="right"> 
                              <p class="sender_name">King Young</p>      
                           </div>
                        </div>
                        <div class="item">
                           <div class="left">
                              <p>To</p>
                           </div>
                           <div class="right">
                              <p class="receiver_name"></p>
                              <p class="phone"></p>
                              <p class="product_info">Got It eGift - 50,000 VND x 1</p>
                              <p class="message"></p>
                           </div>
                        </div>
                        <div class="item">
                           <div class="left">
                              <p>Payment</p>
                           </div>
                           <div class="right">
                              <p class="pay_method">Credit Card</p>
                           </div>
                        </div>
                        <div class="item">
                           <div class="left">
                              <p>Total</p>
                           </div>
                           <div class="right">
                              <p class="total">50,000 VND</p>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <a class="send_another" href="/send-egift">Send another Gift</a>
                        <a class="back_hompage" href="/">Back to hompage</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-3 right child">
            <div class="show_order_mb">
               <a href="javascript:void(0)" class=" en">Show Order Informations</a>
            </div>
            <div class="box-content">
               <h3>Receiver Information</h3>
               <div class="receive-detail-new">
                  <p class="" style=""><label>Name</label> <span class=""></span></p>     
                  <p class=""><label>Channel</label><span class="text-channel">SMS</span></p>
               </div>
            </div>  
            <div class="box-content right-payment-method" style="display: none;">
               <h3>Payment Method</h3>
                <div class="payment-method-detail" id="payment-method-detail">
                     <p class="">
                        <label>Credit Card</label>
                        <span class=""><img src="/img/pay_creditcard.png"></span>
                     </p>                 
                </div>
            </div>         
            <div class="box-content">
               <h3>Order Summary</h3>
               <div class="order-summary" id="order-summary">
                  <p class=""><label>Quantity</label> <span class="">1</span></p>
                  <p class=""><label>Value</label> <span class="">50,000 VND</span></p>
               </div>
               <div class="total">
                  <p class="col-left">Total</p>
                  <p class="col-right">50,000 VND</p>
               </div>
            </div>
            <form action="https://testsecureacceptance.cybersource.com/silent/pay" id="cybersourceForm" method="post" novalidate="" class="ng-untouched ng-pristine ng-valid">
            </form>            
         </div>
         <div class="right-fixed-wrap" style="width: 89.5px;"></div>
      </div>
   </div>
   <div></div>
</section>
@endsection
@section('scripts')
<script>    
   $(document).ready(function() {

     
   
      $('.form-control').change(function(){
      var $this = $(this);
      if(!$this.attr('placeholder')){
          if($this.val())
         $this.addClass('edited')
         else
         $this.removeClass('edited')
      }
        
    });
      $('.form-group input').keypress(function(event) {
         var val = $(this);
         $('.next-write-message').removeClass('disabled');
         $('.next-write-message').click(function(event) {
            $('.write_message').removeClass('active');
            $('.payment_method').addClass('active');
            $('.breadcrumb p.step-3').addClass('active');
            $('.right-payment-method').css({'display':'block'});
         });
      });

        $('.payment-channel li').click(function(event) {
          var a =  $(this).attr('href');
          if(a != '#tab_credit'){
            $('.right-payment-method .payment-method-detail p label').text('ATM');
            $('.right-payment-method .payment-method-detail p span img').attr({
               src: 'img/atm-card.png'
            });
          }else{
            $('.right-payment-method .payment-method-detail p label').text('Credit Card');
            $('.right-payment-method .payment-method-detail p span img').attr({
               src: '/img/pay_creditcard.png'
            });
          }
          
        });
         $('.next-payment').click(function(event) {
            $('.payment_method').removeClass('active');
            $('.finish_box ').addClass('active');
         });


   });
</script>
@endsection