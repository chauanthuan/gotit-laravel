@extends('layouts.master')
@section('styles')
  <link rel="stylesheet" href="{!! asset('css/gift-received.css') !!}">
  <style>
    .modal {
text-align: center;
}

.modal:before {
display: inline-block;
vertical-align: middle;
content: " ";
height: 100%;
}

.modal-dialog {
display: inline-block;
text-align: left;
vertical-align: middle;
}
  </style>
@endsection
@section('content')
<section class="managegift-section section-content">
   <div class="container">
      <div class="row parent" id="parent">
         <div class="col-md-3 left child">
            @include('account.menu-left')
          <!--   <div class="box-content">
               <h3>My Account</h3>
               <div class="account-detail" id="account-detail">
                  <p class=""><a href="/account">Account Setting</a></p>
                  <p class=""><a href="/changepassword">Change Password</a></p>
               </div>
            </div>
            <div class="box-content">
               <h3>History</h3>
               <div class="history-detail" id="history-detail">
                  <p><a href="/gift-received">eGifts Received</a></p>
                  <p><a href="/gift-sent" class="active">eGifts Sent</a></p>
               </div>
            </div> -->
         </div>
         <div class="col-md-9 right child">
            <div class="wrap_gift">
               <div class="box_top">
                  <div class="title">
                     <h2 class="">eGifts Sent</h2>
                     <p class="total_voucher">Found 0 eGifts</p>
                  </div>
                  <div class="filter_box" id="filter">
                    <p class="title_mb">Filter by</p>
                    <div class="filter_selected_mb">
                      <span class="status_active">Status: Active</span>
                      <span class="sortby expiry">Sort by: Expiry</span>
                    </div>
                    <div class="filter_group">
                      <div class="filter_left">
                        <label class="filter_title">Filter by</label>
                        <p>
                          <input name="filterValue[]" type="checkbox" value="3" id="filter_3">
                          <label for="filter_3">Active</label>
                        </p>
                        <p>
                          <input name="filterValue[]" type="checkbox" value="8" id="filter_8">
                          <label for="filter_8">Expired</label>
                        </p>
                        <p>
                          <input name="filterValue[]" type="checkbox" value="4" id="filter_4">
                          <label for="filter_4">Used</label></p>
                      </div>
                      <div class="filter_right">
                        <label class="filter_title">Sort by</label>
                        <div class="filter_right_selectbox" id="filter_right_selectbox">
                          <span class="filter_right_selected">
                            Expiry
                          </span>
                          <div class="dropdown_selected">
                            <p>
                              <input id="sort_expiry" name="sort" type="radio" value="expiry" class="ng-untouched ng-pristine ng-valid">
                              <label for="sort_expiry">Expiry</label>
                            </p>
                            <p>
                              <input id="sort_value" name="sort" type="radio" value="value" class="ng-untouched ng-pristine ng-valid">
                              <label for="sort_value">Value</label>
                            </p>
                            <p>
                              <input id="sort_brand" name="sort" type="radio" value="brand" class="ng-untouched ng-pristine ng-valid">
                              <label for="sort_brand">Brand</label>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
               </div>
                <div class="box_content">
                  <div class="gift_list">
                    <div class="gift_item status_3">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/04/1524796160_Js3Ts.png">
                      </div>
                      <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Ministop
                            
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>Ministop eGift (MoMo)</label>
                            <label> 30,000 VND</label>
                           GOT IT
                           <b>679 048 8875</b>
                          </p>
                          <p class="status">
                            <span>Active</span>
                            <label>Expiry: 31/07/2018</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;"><img src="/img/seen_icon.png"><a target="_blank" href="https://www.gotit.vn/KSJA6">Open eGift</a></label>
                            <label><img src="/img/term-3-x@3x.png"><a href="javascript:void(0)" data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a></label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/KSJA6"></a>
                    </div>
                    <div class="gift_item status_3">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/04/1524796160_Js3Ts.png">
                      </div>
                      <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Ministop
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>Ministop eGift (MoMo)</label>
                            <label> 30,000 VND</label>
                           GOT IT
                           <b>883 422 6312</b>
                          </p>
                          <p class="status">
                            <span>Active</span>
                            <label>Expiry: 31/07/2018</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;">
                              <img src="/img/seen_icon.png">
                              <a target="_blank" href="https://www.gotit.vn/mLiY9">Open eGift</a>
                            </label>
                            <label>
                              <img src="/img/term-3-x@3x.png">
                              <a href="javascript:void(0)" data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a>
                            </label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/mLiY9"></a>
                    </div>
                    <div class="gift_item status_3">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/04/1524796065_uKyb2.png">
                      </div>
                      <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Ministop
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>eGift</label>
                            <label> 30,000 VND</label>
                            GOT IT
                            <b>376 066 0216</b>
                          </p>
                          <p class="status">
                            <span>Active</span>
                            <label>Expiry: 30/08/2018</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;">
                              <img src="/img/seen_icon.png">
                              <a target="_blank" href="https://www.gotit.vn/9Tir3">Open eGift</a>
                            </label>
                            <label>
                              <img src="/img/term-3-x@3x.png">
                              <a href="javascript:void(0)"  data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a>
                            </label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/9Tir3"></a>
                    </div><div class="gift_item status_3">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/04/1524796065_uKyb2.png">
                      </div>
                       <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Ministop
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>eGift</label>
                            <label> 30,000 VND</label>
                           GOT IT
                           <b>294 918 5531</b>
                          </p>
                          <p class="status">
                            <span>Active</span>
                            <label>Expiry: 30/08/2018</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;"><img src="/img/seen_icon.png"><a target="_blank" href="https://www.gotit.vn/CjbMb">Open eGift</a></label>
                            <label><img src="/img/term-3-x@3x.png"><a href="javascript:void(0)" data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a></label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/CjbMb"></a>
                    </div><div class="gift_item status_4">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
                      </div>
                       <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Got It
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>Got It eGift. </label>
                            <label> 50,000 VND</label>
                           GOT IT
                           <b>069 865 4168</b>
                          </p>
                          <p class="status">
                            <span>Used</span>
                            <label>Expiry: 18/04/2018</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;"><img src="/img/seen_icon.png"><a target="_blank" href="https://www.gotit.vn/Kk9Gt">Open eGift</a></label>
                            <label><img src="/img/term-3-x@3x.png"><a href="javascript:void(0)" data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a></label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/Kk9Gt"></a>
                    </div><div class="gift_item status_4">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/04/1524796160_Js3Ts.png">
                      </div>
                       <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Ministop
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>Ministop eGift (MoMo)</label>
                            <label> 30,000 VND</label>
                           GOT IT
                           <b>630 338 9735</b>
                          </p>
                          <p class="status">
                            <span>Used</span>
                            <label>Expiry: 31/07/2018</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;"><img src="/img/seen_icon.png"><a target="_blank" href="https://www.gotit.vn/7Jvpj">Open eGift</a></label>
                            <label><img src="/img/term-3-x@3x.png"><a href="javascript:void(0)" data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a></label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/7Jvpj"></a>
                    </div>
                    <div class="gift_item status_8">
                      <div class="gift_img">
                        <img src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
                      </div>
                       <div class="gift_info">
                        <div class="product_name">
                          <h3>
                            Got It
                          </h3>
                        </div>
                        <div class="content">
                          <p class="received_date">
                            <label>Got It eGift. </label>
                            <label> 50,000 VND</label>
                           GOT IT
                           <b>969 032 5286</b>
                          </p>
                          <p class="status">
                            <span>Expired</span>
                            <label>Expiry: 03/06/2017</label>
                            <label> </label>
                          </p>
                          <p class="expiry_date">
                            <label style="padding-bottom: 0px;"><img src="/img/seen_icon.png"><a target="_blank" href="https://www.gotit.vn/xcftp">Open eGift</a></label>
                            <label><img src="/img/term-3-x@3x.png"><a href="javascript:void(0)" data-target="#term_condition_modal" data-toggle="modal">Terms and Conditions</a></label>
                          </p>
                        </div>
                      </div>
                      <a class="show_voucher" target="_blank" href="https://www.gotit.vn/xcftp"></a>
                    </div>
                  </div>
                  <a class="lazy_load_point" href="javascript:void(0)" style="text-indent: -1e+07px;"></a>
                </div>
            </div>
         </div>
         <div class="left-fixed-wrap" style="width: 89.5px;"></div>
      </div>
   </div>
   <div aria-hidden="true" aria-labelledby="mySmallModalLabel" bsmodal="" class="modal fade" id="notificationModal" role="dialog" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" style="overflow:hidden">
               <h4 class="modal-title pull-left">Notification</h4>
               <button aria-label="Close" class="close pull-right" type="button">
               <span aria-hidden="true">×</span>
               </button>
            </div>
            <div class="modal-body">
               <p></p>
            </div>
         </div>
      </div>
   </div>
   <div id="term_condition_modal" class="modal fade" bsModal #termconditionModal="bs-modal"
   tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
   <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="overflow:hidden">
        <h4 class="modal-title pull-left">Terms and Conditions</h4>
        <button type="button" class="close pull-right" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div  class="content">
          <p>- eGift is valid for one-time use only.</p>

          <p>- Multiple eGift can be applied on one bill</p>

          <p>- Please refer to the expiry date on the eGift.</p>

          <p>- eGift is only applicable for appointed products or services. In case appointed products or services are not available, customers may have choices for products or services that have same value.</p>

          <p>- eGift must be showed to the cashier to redeem products or services. In case customers are not sure whether using the eGift or not, please do not present it to the cashier.</p>

          <p>- eGift are non-refundable and cannot be resold.</p>

          <p>- eGift are not exchangeable for cash in part or whole. If the amount of your order exceeds the - value of the eGift, you will be required to pay for the balance.</p>

          <p>- You are responsible for keeping your eGift code safe and secret. Got It is not responsible if the eGift is lost, or used without permission and no replacement will be provided in these circumstances.</p>

          <p>- Got It is not responsible for and shall have no liability relating to the quality of any products or services provided by the merchants.</p>

          <p>- Got It reserves the rights to amend the terms and conditions at any time without notice.</p>
        </div>
      </div>
      <!-- <div class="modal-footer"> -->
        <!-- <button type="button" class="btn btn-default" (click)="termconditionModal.hide()">Cancel</button> -->
        <!-- </div> -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
     $('#history-detail p.gift-received a').addClass('active');

    var w_width = $(window).width();
    var w_height = $(window).height();
    var footer_height = $('footer').height();
    $('.managegift-section .parent').css({'min-height':(w_height - footer_height - 63)+'px'})
    
    var content_width = $('.managegift-section .parent').width();
    var path_right_width = (w_width - content_width)/2;
    $('.left-fixed-wrap').css({'width':path_right_width});


    $(window).resize(function(){
      var w_width = $(window).width();
      var w_height = $(window).height();
      var footer_height = $('footer').height();
      $('.managegift-section .parent').css({'min-height':(w_height - footer_height - 63)+'px'});

      var content_width = $('.managegift-section .parent').width();
      if(w_width >= content_width){
        var path_right_width = (w_width - content_width)/2;
      }
      else{
        path_right_width = 0;
      }
      $('.left-fixed-wrap').css({'width':path_right_width});
    })
  })

</script>
@endsection