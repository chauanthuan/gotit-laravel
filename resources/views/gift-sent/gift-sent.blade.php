@extends('layouts.master')
@section('styles')
  <link rel="stylesheet" href="{!! asset('css/gift-sent.css') !!}">
@endsection
@section('content')
<section class="managegift-section section-content">
   <div class="container">
      <div class="row parent" id="parent">
         <div class="col-md-3 left child">
           <!--  <div class="box-content">
               <h3>My Account</h3>
               <div class="account-detail" id="account-detail">
                  <p class=""><a href="/account">Account Setting</a></p>
                  <p class=""><a href="/changepassword">Change Password</a></p>
               </div>
            </div>
            <div class="box-content">
               <h3>History</h3>
               <div class="history-detail" id="history-detail">
                  <p><a href="/gift-received">eGifts Received</a></p>
                  <p><a href="/gift-sent" class="active">eGifts Sent</a></p>
               </div>
            </div> -->
              @include('account.menu-left')
         </div>
         <div class="col-md-9 right child">
            <div class="wrap_gift">
               <div class="box_top">
                  <div class="title">
                     <h2 class="">eGifts Sent</h2>
                     <p class="total_voucher">Found 0 eGifts</p>
                  </div>
                  <div class="filter_box" id="filter">
                     <p class="title_mb">Filter by</p>
                     <div class="filter_selected_mb">
                     </div>
                     <div class="filter_group">
                        <div class="filter_left">
                           <label>Filter by</label>
                           <p><input name="filterValue[]" type="checkbox" value="3" id="filter_3"><label for="filter_3">Sent</label></p>
                           <p><input name="filterValue[]" type="checkbox" value="2" id="filter_2"><label for="filter_2">Purchased</label></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="box_content">
                <div class="gift_list">
                  <div class="gift_item ">
                    <div class="gift_img">
                      <img onerror="this.src='https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png'" src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
                    </div>
                    <div class="gift_info">
                      <div class="product_name">
                        <h3>
                          Got It
                        </h3>
                      </div>
                      <div class="content">
                        <p class="received_date">
                          <label>Quà tặng Got It</label>
                          <label>50,000 VND</label>
                          <label>2018-06-04 14:13</label>
                        </p>
                        <p class="status">
                          <label class="receiver_name">Châu</label>
                        </p>
                        <p class="expiry_date">
                          <label><img src="/img/forward-icon@3x.png">
                            <a href="javascript:void(0)">Gửi lại</a>
                          </label>
                          <label class="purchase-label"><img src="/img/cart-icon@3x.png"><a ng-reflect-router-link="/send-egift" href="/send-egift">Tặng tiếp</a></label>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="gift_item ">
                    <div class="gift_img">
                      <img onerror="this.src='https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png'" src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
                      <span class="vc_state used">Đã dùng</span>
                    </div>
                    <div class="gift_info">
                      <div class="product_name">
                        <h3>
                          Got It
                        </h3>
                      </div>
                      <div class="content">
                        <p class="received_date">
                          <label>Quà tặng Got It</label>
                          <label>50,000 VND</label>
                          <label>2018-06-04 14:20</label>
                        </p>
                        <p class="status">
                          <span class="sms"><small>Gửi bằng</small> sms</span>
                          <label class="receiver_name">Châu<label class="receiver_em_sms">
                            01694951380
                          </label>
                        </p>
                        <p class="expiry_date">
                          <label><img src="/img/forward-icon@3x.png">
                           <a href="javascript:void(0)">Gửi lại</a>
                         </label>
                         <label class="purchase-label"><img src="/img/cart-icon@3x.png"><a ng-reflect-router-link="/send-egift" href="/send-egift">Tặng tiếp</a></label>
                        </p>
                      </div>
                    </div>
                  </div>
                <div class="gift_item ">
                  <div class="gift_img">
                    <img onerror="this.src='https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png'" src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
                    <span class="vc_state sent">Đã gửi</span>
                  </div>
                  <div class="gift_info">
                    <div class="product_name">
                      <h3>
                        Got It
                      </h3>
                    </div>
                    <div class="content">
                      <p class="received_date">
                        <label>Quà tặng Got It</label>
                        <label>50,000 VND</label>
                        <label>2018-06-04 14:02</label>
                      </p>
                      <p class="status">
                        <span class="sms"><small>Gửi bằng</small> sms</span>
                        <label class="receiver_name">Châu</label>
                        <label class="receiver_em_sms">
                          01694951380
                        </label>
                      </p>
                      <p class="expiry_date">
                        <label><img src="/img/forward-icon@3x.png">
                          <a href="javascript:void(0)">Gửi lại</a>
                        </label>
                        <label class="purchase-label"><img src="/img/cart-icon@3x.png"><a ng-reflect-router-link="/send-egift" href="/send-egift">Tặng tiếp</a></label>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="gift_item ">
                  <div class="gift_img">
                    <img onerror="this.src='https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png'" src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
                    <span class="vc_state expired">Hết hạn</span>
                  </div>
                    <div class="gift_info">
                      <div class="product_name">
                        <h3>
                          Got It
                        </h3>
                      </div>
                      <div class="content">
                        <p class="received_date">
                          <label>Quà tặng Got It</label>
                          <label>50,000 VND</label>
                          <label>2018-06-04 14:15</label>
                        </p>
                        <p class="status">
                          <label class="receiver_name">Châu</label>
                        </p>
                        <p class="expiry_date">
                          <label><img src="/img/forward-icon@3x.png">
                           <a href="javascript:void(0)">Gửi lại</a>
                         </label>
                         <label class="purchase-label"><img src="/img/cart-icon@3x.png"><a ng-reflect-router-link="/send-egift" href="/send-egift">Tặng tiếp</a></label>
                       </p>
                     </div>
                    </div>
                  </div>
                   <!-- <div class="tab-content" style="height: 300px; position: relative;">
                      <p style="text-align:center;color:red;position:absolute;left:50%;top:50%;transform:translate(-50%,-50%)">
                         You do not have a eGift.
                      </p>
                   </div> -->
                  </div>
                  <a class="lazy_load_point" href="javascript:void(0)" style="text-indent: -1e+07px;"></a>
               </div>
            </div>
         </div>
         <div class="left-fixed-wrap" style="width: 89.5px;"></div>
      </div>
   </div>
   <div aria-hidden="true" aria-labelledby="mySmallModalLabel" bsmodal="" class="modal fade" id="notificationModal" role="dialog" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" style="overflow:hidden">
               <h4 class="modal-title pull-left">Notification</h4>
               <button aria-label="Close" class="close pull-right" type="button">
               <span aria-hidden="true">×</span>
               </button>
            </div>
            <div class="modal-body">
               <p></p>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
     $('#history-detail p.gift-sent a').addClass('active');

    var w_width = $(window).width();
    var w_height = $(window).height();
    var footer_height = $('footer').height();
    $('.managegift-section .parent').css({'min-height':(w_height - footer_height - 63)+'px'})
    
    var content_width = $('.managegift-section .parent').width();
    var path_right_width = (w_width - content_width)/2;
    $('.left-fixed-wrap').css({'width':path_right_width});


    $(window).resize(function(){
      var w_width = $(window).width();
      var w_height = $(window).height();
      var footer_height = $('footer').height();
      $('.managegift-section .parent').css({'min-height':(w_height - footer_height - 63)+'px'});

      var content_width = $('.managegift-section .parent').width();
      if(w_width >= content_width){
        var path_right_width = (w_width - content_width)/2;
      }
      else{
        path_right_width = 0;
      }
      $('.left-fixed-wrap').css({'width':path_right_width});
    })
  })

</script>
@endsection