@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/forgotpassword.css') !!}">
<style type="text/css">
	footer{
		display: none;
	}
</style>
@endsection
@section('content')
<section class="forgotpassword">
	<div class="container">
		<div class="row">
			<div class="content">
				<form action="#" autocomplete="off" class="forgotpassword-form" method="post" novalidate="">
					<div class="form-title">
						<h2>Forgot password</h2>
						<span class="form-subtitle">Just remember? <a href="/login">Login here</a> 
						</span>
					</div>
					<div class="form-group form-md-floating-label">
						<input autocomplete="off" id="email" name="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" required="" type="text" class="form-control">
						<label for="email">Email</label>
					</div>
					<div class="form-actions">
						<button class="btn btn-submit" type="submit">Send</button>
					</div>			
				</form>
			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.form-control').change(function(){
			var $this = $(this);
			if($this.val())
				$this.addClass('edited')
			else
				$this.removeClass('edited')
		});
		
		var w_height = $(window).height();
		$('.forgotpassword').css({'min-height':(w_height - 63)+'px'});
		$(window).resize(function(){
			var w_height = $(window).height();
			$('.forgotpassword').css({'min-height':(w_height - 63)+'px'});
		})
		
	})
</script>
@endsection