@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/login.css') !!}">
<style type="text/css">
	footer{
		display: none;
	}
</style>
@endsection
@section('content')
<section class="login section-content">
	<div class="container">
		<div class="row">
			<div class="content">
				<form action="#" autocomplete="off" class="login-form" method="post" novalidate="">
					<div class="form-title">
						<h2 _ngcontent-c3="">Log In</h2>
						<span class="form-subtitle"> Don’t have an account? <a href="/signup">Create your account</a> now! It takes less than a minute. </span>
					</div>
					<div class="col-md-12">
						<div class="row">
						
                      	</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group form-md-floating-label">
						<input autocomplete="off" id="email" name="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" required="" type="text" class="form-control">
						<label for="email">Email</label>
					</div>
					<div class="form-group form-md-floating-label">
						<input autocomplete="password" name="password" required="" type="password" class="form-control">
						<label for="password">Password</label>
					</div>
					<div class="form-actions">
						<div class="pull-left">
							<input id="remember" name="remember2" type="checkbox" value="1">
							<label class="rememberme check" for="remember">Remember me </label>
						</div>
						<div class="pull-right forget-password-block">
							<a class="forget-password" href="/forgotpassword" id="forget-password">Forgot password</a>
						</div>
					</div>
					<div class="form-actions">
						<button class="btn btn-submit" type="submit">Log In</button>
					</div>
					<div class="login-options">
						<h4 class="">Or</h4>
						<div class="group_button">
		                    <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:void(0);">Login with Facebook</a>
		                    <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:void(0);">Login with Google</a>
		                </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.form-control').change(function(){
			var $this = $(this);
			if($this.val())
				$this.addClass('edited')
			else
				$this.removeClass('edited')
		});
		var w_width = $(window).width();
		var w_height = $(window).height();

		$('.section-content').css({'min-height':(w_height - 63)+'px'});


		$(window).resize(function(){
			var w_width = $(window).width();
			var w_height = $(window).height();
			$('.section-content').css({'min-height':(w_height - 63)+'px'});
		})
	})
</script>
@endsection