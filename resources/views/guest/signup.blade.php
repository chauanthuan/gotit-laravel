@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/signup.css') !!}">
<style type="text/css">
	footer{
		display: none;
	}
</style>
@endsection
@section('content')
<section class="signup section-content">
	<div class="container">
		<div class="row">
			<div class="content">
				<form action="#" autocomplete="off" class="signup-form ng-pristine ng-invalid ng-touched" method="post" ngnativevalidate="" novalidate="novalidate">
					<div class="form-title">
						<h2>Register</h2>
						<span class="form-subtitle">Signing up is free and always will be! Register with us</span>
					</div>
					
				<!-- 	<p class="help-block error-msg">
		            Phone is required.
		            </p> -->
					
					<div class="group_two_col">
						<div class="form-group form-md-floating-label left">
							<input id="firstname" name="firstname" required="" type="text" class="form-control" value="">
							<label for="form_control_1">Firstname</label>
						</div>
						<div class="form-group form-md-floating-label right">
							<input id="lastname" name="lastname" required="" type="text" class="form-control">
							<label for="lastname"> Lastname </label>
						</div>
					</div>
					<div class="form-group form-md-floating-label">
						<input autocomplete="off" id="email" name="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" required="" type="email" class="form-control">
						<label for="email">Email</label>
					</div>

					<div class="form-group form-md-floating-label">
						<input autocomplete="off" id="phone" maxlength="11" minlength="10" name="phone" pattern="^(09|08|01[2689])\d{8}$" required="" type="text" class="form-control ng-pristine ng-invalid ng-touched">
						<label for="phone"> Phone Number</label>
					</div>
					<div class="group_two_col">
						<div class="form-group form-md-floating-label left">
							<input id="password" minlength="6" name="password" required="" type="password" class="form-control">
							<label for="password">Password</label>
						</div>
						<div class="form-group form-md-floating-label right">
							<input id="confirmpassword" name="confirmpassword" required="" type="password" class="form-control">
							<label for="confirmpassword">Confirm password</label>
						</div>
					</div>
					
					
					<div class="form-actions">
						<button class="btn btn-submit" type="submit">Register</button>
					</div>
					
					<div class="signup-options">
						<h4 class="pull-left">Or Sign Up with</h4>
						<ul class="social-icons pull-right">
							<li>
								<a class="social-icon-color facebook" data-original-title="facebook" href="javascript:void(0);"></a>
							</li>
							
							<li>
								<a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:void(0);"></a>
							</li>
							
						</ul>
					</div>
				</form>
			</div>
		</div>
	</div>

<div aria-hidden="true" aria-labelledby="mySmallModalLabel" bsmodal="" class="modal fade" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="overflow:hidden">
        <h4 class="modal-title pull-left">Notification</h4>
       
      </div>
      <div class="modal-body">
      	<p>An account already exists with the same email address but different sign-in credentials. Please enter your password with this email address. System will <span style="color:#ff5f5f">auto redirect after 10s</span>.</p>
      	<p>Email trong FB này đã được đăng kí với hệ thống bằng phương thức truyền thống. Vui lòng nhập mật khẩu cho email này. Hệ thống <span style="color:#ff5f5f">tự chuyển sau 10s</span>.</p>
        <input autofocus="" class="form-control" id="passwordRequired" name="passwordRequired" placeholder="Your password" type="password">
      </div>
      
        
        
      
    </div>
  </div>
</div>

</section>
@endsection
@section('scripts')
<script>
	$(document).ready(function() {
		$('.form-control').change(function(){
			var $this = $(this);
			if($this.val())
				$this.addClass('edited')
			else
				$this.removeClass('edited')
		});

		var w_width = $(window).width();
		var w_height = $(window).height();

		$('.section-content').css({'min-height':(w_height - 63)+'px'});


		$(window).resize(function(){
			var w_width = $(window).width();
			var w_height = $(window).height();
			var footer_height = $('footer').height();
			$('.section-content').css({'min-height':(w_height - 63)+'px'});
		})
	});
</script>
@endsection
