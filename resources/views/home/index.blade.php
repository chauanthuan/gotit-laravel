@extends('layouts.master')
@section('content')
<section class="introdution-section">
	<div class="container">
	    <div class="row">
	      <div class="col-md-7 col-sm-8 text-intro">
	        <h2>Send An eGift Voucher Instantly</h2>
	        <p>Got It voucher Are Sent Instantly And The Recipient Can Use Immediately.</p>
	        <div class="img_bg_mobile">
	            <img src="/img/product-img-mobile-new.png">
	        </div>
	        <div class="btn_group">
	          <a class="btn-lg btn-send" href="/send-egift">
	          SEND A GIFT
	          </a>
	          <a class="btn-lg btn-manage" href="/gift-received">MANAGE YOUR GIFTS
	          </a>
	        </div>
	      </div>
	      <div class="wrap_intro_img">
	        <img src="/img/product_img_new.png" width="100%">
	      </div>
	    </div>
	</div>
</section>
@include('shared.listbrand')
<section class="bussiness-section">
  <div class="container">
    <div class="row">
      <div class="col-md-6 bussiness-intro">
        <h2>Got It for Business</h2>
        <p>Let Got It take away your gifting hassles.  Our easy to use site allows you to send bulk gifts instantly</p>
        <div class="img_bg_mobile">
            <img src="/img/bg_business_chart3x.png">
        </div>
        <a href="https://biz.gotit.vn" target="_blank">Explore Now</a>
      </div>
      
    </div>
  </div>
  <div class="wrap_img">
    <img src="/img/bg_business_chart3x.png" style="max-width:none">
  </div>
</section>
<section class="accordion-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="box_top tabbable-line">
            <h2 class="pull-left">Announcement</h2>
        </div>
        <div class="box_content">
          <div class="row">
            <div class="col-md-12">
                <div class="box-item" style="margin-bottom: 20px;">
                    <h4>07 Mar 2018 - Merchant - Welcome the new brand: Manwah - Taiwanese Hot Pot joins Got It</h4>
                    <p>The Golden Gate Group's Manwah Restaurant offers a new hot pot experience in Vietnam. At Manwah, diners can easily explore an wonderful culinary journey which is an combination of mellowy juice and featured Taiwanese spices.</p>
                </div>
                <div class="box-item" style="margin-bottom: 20px;">
                    <h4>05 Mar 2018 - Merchant - Welcome the new brand: XOX - fashioned bag brand joins Got It</h4>
                    <p>Bring the art into every single product, each XOX bag is a unique picture. Premium products are made from environment-friendly, lightweight and easy cleanning Polyeste material. XOX bags will suit you on many occasions.</p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection