<html lang="{{ Cookie::get('laravel_language', 'vi') }}">
<head>
	<title>Got It</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{!!  csrf_token()  !!}" />
    <meta http-equiv="content-language" content="vi">
    <link rel="shortcut icon" href="{!!  asset('favicon.png')  !!}">
    <meta property="fb:app_id" content="460418860782124" />
    <meta name="copyright" content="Got It"/>
    <meta name="description" content="Got It"/>
    <meta name="keywords" content="quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân"/>

    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-select.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/style-icon.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/non-responsive.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/menu.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/main.css') !!}">

    @yield('styles')

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{!! asset('js/jquery.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
    <script src="{!! asset('js/main.js') !!}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('scripts')
</head>
<body>
    @include('shared/header')
    @yield('content')
    @include('shared/footer')
    <div id="fb-root"></div>
</body>
</html>