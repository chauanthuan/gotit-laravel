@extends('layouts.master')
@section('styles')
  <link rel="stylesheet" href="{!! asset('css/product-detail.css') !!}">
@endsection

@section('content')
<section class="product-section">
	<div class="container">
		<div class="row parent" id="parent">
			<div class="col-md-9 left child">
				<div class="breadcrumb">
					<p class="step-1 active"><span></span>Choose eGift</p>
					<p class="step-2"><span></span>Enter receiver detail and your message</p>
					<p class="step-3 "><span></span>Payment</p>
				</div>
				<div class="product-info">
					<div class="product_name">
						<h3>Got It eGift</h3>
						<p>Send eGift Voucher Instantly.</p>
					</div>
					<div class="show-img">
						<div class="main-img">
							<img height="" src="https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png">
							<span class="price_pr">50,000 VND</span>
						</div>         
					</div>
					<div class="product-detail">
						<div class="choose-product-type">
							<p class="text-label">Choose value</p>
							<div class="wrap-pr-type">
								<ul>
									<li>
										<input class="" name="product" type="radio" id="50000" checked="checked" value="50000">
										<label class="" for="50000">{!! Currency::formatMoney('50000').' VND' !!}</label>
									</li>
									<li>
										<input class="" name="product" type="radio" id="100000" value="100000">
										<label class="" for="100000">{!! Currency::formatMoney('100000').' VND' !!}</label>
									</li>
									<li>
										<input class="" name="product" type="radio" id="200000" value="200000">
										<label class="" for="200000">{!! Currency::formatMoney('200000').' VND' !!}</label>
									</li>
									<li>
										<input class="" name="product" type="radio" id="500000" value="500000">
										<label class="" for="500000">{!! Currency::formatMoney('500000').' VND' !!}</label>
									</li>
									<li>
										<input class="" name="product" type="radio" id="1000000" value="1000000">
										<label class="" for="1000000">{!! Currency::formatMoney('1000000').' VND' !!}</label>
									</li>
								</ul>
							</div>
						</div>
						<div class="choose-quantity">
							<p class="text-label">Choose quantity</p>
							<div class="select-quantity">
								<div class="input-group">
									<span class="input-group-btn">
										<button class="btn btn-default adjust-product-btn" data-action="minus" id="btn-minus" type="button"><span class="icon-minus"></span></button>
									</span>
									<input class="form-control" id="qty" name="qty" type="text" value="1">
									<span class="input-group-btn">
										<button class="btn btn-default adjust-product-btn" data-action="plus" id="btn-plus" type="button"><span class="icon-plus"></span></button>
									</span>
								</div>
							</div>
						</div>
						<div class="sumary">
							<span class="pull-right text-right">Total: {!! Currency::formatMoney('50000').' VND' !!}</span>
							<a class="btn-send-gift" href="/checkout">Next step</a>
						</div>
					</div>
				</div>
    
				<div class="store-info">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="title">
								<h2>Store Locations</h2>
                                <a class="find_nearest_store" href="javascript:void(0)"><span class="icon-location"></span> Find nearest location</a>
							</div>
						</div>
						<div class="panel-body own-panel-body maps_data">
							<div id="embed-map"></div>
							<div class="searchMap">
								<div class="brand">
									<p>Brand</p>
									<select class="selectpicker select_brand form-control" data-size="5" data-live-search="true">
			                            <option>All</option>
			                            <option>February</option>
			                            <option>March</option>
			                            <option>April</option>
			                            <option>March</option>
			                            <option>May</option>
			                            <option>June</option>
			                            <option>July</option>
			                            <option>August</option>
			                            <option>September</option>
			                            <option>October</option>
			                            <option>November</option>
			                          </select>
								</div>
								<div class="city">
									<p>City</p>
									<select class="selectpicker select_city form-control" data-size="5" data-live-search="true">
			                            <option>All</option>
			                            <option>February</option>
			                            <option>March</option>
			                            <option>April</option>
			                            <option>March</option>
			                            <option>May</option>
			                            <option>June</option>
			                            <option>July</option>
			                            <option>August</option>
			                            <option>September</option>
			                            <option>October</option>
			                            <option>November</option>
			                          </select>
								</div>
								<div class="district">
									<p>District</p>
									<select class="selectpicker select_district form-control" data-size="5" data-live-search="true">
			                            <option>All</option>
			                            <option>February</option>
			                            <option>March</option>
			                            <option>April</option>
			                            <option>March</option>
			                            <option>May</option>
			                            <option>June</option>
			                            <option>July</option>
			                            <option>August</option>
			                            <option>September</option>
			                            <option>October</option>
			                            <option>November</option>
			                          </select>
								</div>
								<div class="searchMapBtn">
									<a href="javascript:void(0)" (click)="applyFilter($event)">Apply</a>
								</div>
								<!-- </div> -->
							</div>
						</div>
              		</div>
      			</div>
			    <div class="brand-info">
					<div class="col-md-12 brand_productpage">
						<div class="box_top tabbable-line">
							<h2 class="pull-left">Our Brands</h2>
							<div class="pull-right dropdown-mobile wrapper-dropdown" id="tab_all_brand" tabindex="1">
								<span>Category<small>All categories</small></span>
								<ul class="nav nav-tabs en">
									<li class="active">
										<a aria-expanded="false" data-id="0" data-toggle="tab" href="#tab_1">
										All categories</a>
									</li>
									<li>
										<a aria-expanded="true" data-id="2" data-toggle="tab" href="#tab_2">
										Fastfood &amp; Café </a>
									</li>
									<li class="">
										<a aria-expanded="false" data-id="1" data-toggle="tab" href="#tab_3">
										Restaurants </a>
									</li>
									<li style="display: none;">
										<a aria-expanded="true" data-id="3" data-toggle="tab" href="#tab_4">
										Fashion &amp; Cosmetic </a>
									</li>
									<li class="">
										<a aria-expanded="false" data-id="7" data-toggle="tab" href="#tab_5">
										Mall &amp; Mart </a>
									</li>
								</ul>
					        </div>
						</div>
						<div class="box-content">
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
										</a>
									</div>
									<p class="show-more">
										<a class="btn-show-more" href="javascript:void(0)">Show more<span></span></a>
									</p>
								</div>
								<div class="tab-pane " id="tab_2">
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
										</a>
									</div>
									<p class="show-more">
										<button class="btn btn-show-more">Show more</button>
									</p>
								</div>
								<div class="tab-pane" id="tab_3">
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
										</a>
									</div>
									<p class="show-more">
										<button class="btn btn-show-more">Show more</button>
									</p>
								</div>
								<div class="tab-pane" id="tab_4">
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
										</a>
									</div>
									<p class="show-more">
										<button class="btn btn-show-more">Show more</button>
									</p>
								</div>
								<div class="tab-pane" id="tab_5">
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
										</a>
									</div>
									<div class="list_brand">
										<a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
										</a><a class="brand_img" href="javascript:void(0)">
											<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
										</a>
									</div>
									<p class="show-more">
										<button class="btn btn-show-more">Show more</button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 right child">
				<div class="box-content">
					<h3 class="">How it works</h3>
					<div class="how-it-work collapse in show" id="how-it-work" aria-expanded="true" aria-hidden="false" style="overflow: visible; height: auto; display: block;">
						<div><p>- Please see the brand list and store list below.</p></div>
					</div>
				</div>
				<div class="box-content">
					<h3 class="">Terms and Conditions</h3>
					<div class="term_service collapse in show" id="term_service" aria-expanded="true" aria-hidden="false" style="overflow: visible; height: auto; display: block;">
						<div>
							<p>Got It gift voucher is available at Got It's partner stores.</p>

							<p>Along with Got It to show the caring with variety and quality gifts from reputable brands.</p>

							<p>Gifts are sent instantly and conveniently via Facebook, Sms, Email or Viber, Whatsapp.</p>

							<p><strong>Got It Voucher is applicable with promotion at:</strong></p>

							<p>Baskin Robbins, Beard Papa's, Plus One, BreadTalk, The Sushi Bar, ThaiExpress, Ngo-Restaurant, Shabu Ya, Spice Temple, Yves Rocher France, Lock&amp;Lock, SumoBBQ, GoGi House, Kichi Kichi, Daruma, Ashima, iSushi, Ba Con Cừu, Vuvuzela, K-Pub, Crystal Jade Kitchen, Cowboy's Jack, City Beer Station, Hutong, 37th Street, Kintaro Udon, Magic Pan, Sừng Quăn, iCook, Shogun, Highlands Coffee, Hoang Yen Buffet, Hoang Yen Buffet Premier, Hoang Yen Hot Pot, Hoang Yen Vietnamese Cuisine, Stix, Ding Tea, John Henry, Aino Sofia, Freelancer.</p>

							<p>- TLJ: applicable for combo and gift promotion, not applicable for discount promotion.</p>

							<p>- Shop&amp;Go: applicable for all products, except top-up phone card, cigarettes, wine, Payoo.</p>

							<p>- Vera, WoW, Jockey: applicable for combo and gift promotion, not applicable for discount promotion.</p>

							<p><strong>Got It Voucher is not applicable with promotion at:</strong></p>

							<p>- Phuc Long, Catherine Denoual Maison, Glow Sky Bar, Urban Kitchen Bar, Shooz, Geox.</p>

							<p><strong>Using at Lotte Mart, please note:</strong></p>

							<p>- eGift is valid for one-time use only.It can be applied maximum 10 eGift code at once for 1 bill (applied for all eGift face value). Please ask for separating bill if you want to use more than 10 eGift at once.</p>

							<p>- Lotte Mart do not accept any returns, refunds or cancellation for bill paid by Got It eGift.</p>

							<p>- Lotte Mart do not return the balance amount between eGift and bill value.</p>

							<p>- Lotte Mart do not issue red invoice for bill paid or partly paid by Got It eGift, including exceeding amount paid by other payment methods (such as: cash, credit card,..)</p>

							<p>- In case clients require red invoice for exceeding amount paid by other payment methods except Got It, please inform Lotte Mart Cashier to proceed. Lotte Mart will refuse any inquiries to issue red invoice for orders including payment made by Got It.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="right-fixed-wrap" style="width: 112.5px;"></div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdv9P3zaD1_MriCFU0edwHwBp4Jk3pG2w&amp;v=3.exp&amp;libraries=places"></script>
<script type="text/javascript">
	
	<?php
        $googleMaps = array();
        foreach ($product->stores() as $store) {
            $googleMaps[] = array(
                'name'  => Translate::transObj($store, 'name'),
                'brand_name' => Translate::transObj($store, 'brand_name'),
                'lat'   => $store->lat,
                'lng'   => $store->long,
                'address'   =>  Translate::transObj($store, 'address'),
                'image'   =>  'https://www.gotit.vn/layouts/v3/images/landmark.png'
            );
        }
        echo 'var mapStores = '.json_encode($googleMaps);
    ?>
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var w_width = $(window).width();
	    var w_height = $(window).height();
	    var footer_height = $('footer').height();
	    $('.product-section .parent').css({'min-height':(w_height - footer_height - 63)+'px'})
	    
	    var w_width = $(window).width();
	    var content_width = $('.product-section .parent').width();
	    if (w_width >= content_width) {
	      var path_right_width = (w_width - content_width) / 2;
	    }
	    else {
	      path_right_width = 0;
	    }
	    $('.right-fixed-wrap').css({'width': path_right_width});

		googleMap.init();

		//iput price change
		$('input[name=product]').change(function() {
			var priceVl = parseInt($(this).val());
			//check value in quantity
			var inputQty = $('.choose-quantity input#qty');
			var quantity = parseInt(inputQty.val());
			$('.product-detail .sumary span').text("Total: "+formatNumber(priceVl*quantity)+' VND');
		});
		//input quantity change
		$('body').on('click', '.adjust-product-btn', function(e) {
			var input = $('.choose-quantity input#qty');
			var quantity = parseInt(input.val());

			if ($(this).data('action') == 'plus') {
					quantity = quantity + 1;

			} else if ($(this).data('action') == 'minus') {
				if(quantity > 1){
					quantity = quantity - 1;
				}
			}
			input.val(quantity);

			//get value radio button price checked
			var priceVl = parseInt($('input[name="product"]:checked').val());
			$('.product-detail .sumary span').text("Total: "+formatNumber(priceVl*quantity)+' VND');
		});

		$(".choose-quantity input#qty").blur(function(){
		    var quantity = parseInt($(this).val());
		    if(quantity == 0){
				quantity = 1;
				$('.choose-quantity input#qty').val(quantity);
			}
		    //get value radio button price checked
			var priceVl = parseInt($('input[name="product"]:checked').val());
			$('.product-detail .sumary span').text("Total: "+formatNumber(priceVl*quantity)+' VND');
		});
	});
	


    $(window).resize(function(){
		var w_width = $(window).width();
		var w_height = $(window).height();
		var footer_height = $('footer').height();
		$('.product-section .parent').css({'min-height':(w_height - footer_height - 63)+'px'});

      	var w_width = $(window).width();
	    var content_width = $('.product-section .parent').width();
	    if (w_width >= content_width) {
	      var path_right_width = (w_width - content_width) / 2;
	    }
	    else {
	      path_right_width = 0;
	    }
	    $('.right-fixed-wrap').css({'width': path_right_width});
    });

    function formatNumber(number)
	{
	    var number = parseInt(number).toFixed(2) + '';
	    var x = number.split('.');
	    var x1 = x[0];
	    var x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + '.' + '$2');
	    }
	    return x1;
	}

var lang = $('html').attr('lang');
var direct_text = 'Chỉ đường';
if(lang != 'vi'){
        direct_text = 'Get directions';
    }

//google mapS
var googleMap = {
    _gMap: Object,
    _gMarker: Object,
    _infoWindow: Object,
    _arrGMarker: [],
    _storeList : [],

    _initLat : '10.7694307' ,
    _initLng: '106.6854644',

    init: function () {
        /*var dataItem = {};
        dataItem.name = 'Demo';
        dataItem.lat    = '10.7887048';
        dataItem.lng    = '106.69275500000003';
        googleMap._storeList.push(dataItem);*/
        googleMap._storeList = mapStores;

        googleMap.initialize();
        //console.log(googleMap._storeList);
        //$('.js-btn-gmap').on('click', googleMap.initialize);
        //$('.js-list-stores .slide').on('click', googleMap.goToMarker);
       // $('.goto-location').on('click', googleMap.goToMarker);
        $('.list-add ul').on('click','li .goto-location', googleMap.goToMarker);

    },
    initialize: function () {

        if (typeof google != 'undefined') {

            var _location = new google.maps.LatLng( googleMap._initLat, googleMap._initLng );
            var _mapOptions = {center: _location,zoom: 15,scrollwheel: false};
            googleMap._gMap = new google.maps.Map(document.getElementById("embed-map"), _mapOptions);

            if (googleMap._storeList.length > 0) {

                googleMap._infoWindow = new google.maps.InfoWindow();
                var bounds = new google.maps.LatLngBounds();

                for (i = 0; i < googleMap._storeList.length; i++) {

                    var _store = googleMap._storeList[i];
                    if (_store.lat != '' && _store.lng != '') {
                        var pos = new google.maps.LatLng(_store.lat, _store.lng);
                        bounds.extend(pos);

                        googleMap._gMarker = new google.maps.Marker({ 
                        	optimized: false, 
                        	title: _store.name, 
                        	position: pos, 
                        	map: googleMap._gMap, 
                        	// icon: _store.image,
                        	icon: {
							    url: _store.image,
							    scaledSize: new google.maps.Size(20, 20)
							} 
                        });
                        google.maps.event.addListener(googleMap._gMarker, 'click', (function (marker, i) {
                            return function () {
                                googleMap._gMap.setCenter(marker.getPosition());
                                var browser = navigator.userAgent;
                                if(browser.match(/(iPhone|iPod|iPad)/)){
                                    var link = 'comgooglemaps://?daddr='+googleMap._storeList[i].address+'&directionsmode=driving';
                                } 
                                else if(browser.match(/Android/)){
                                    var link ='google.navigation:q='+googleMap._storeList[i].address+'&'+googleMap._storeList[i].lat+','+googleMap._storeList[i].long;
                                }

                                googleMap._gMap.setZoom(15);
                                googleMap._infoWindow.setContent('<div class="wrap_marker">'+
                                                                    '<h4 class="header">' + googleMap._storeList[i].brand_name +' - '+ googleMap._storeList[i].name + '</h4>' + 
                                                                    
                                                                    '<div class="subtitle"><p class="address">' + googleMap._storeList[i].address + '</p></div>'+
                                                                    '<div class="subtitle"><a target="_blank" href="'+(link ? link : 'https://www.google.com/maps/dir/'+googleMap._storeList[i].address+'')+'"><p class="get_direction">'+direct_text+'</p></a></div>'+
                                                                '</div>');
                                
                                google.maps.event.addListener(googleMap._infoWindow, 'domready', function() {

                                    // Reference to the DIV which receives the contents of the infowindow using jQuery
                                    var iwOuter = $('.gm-style-iw');

                                    //get parent div
                                    iwOuter.parent().addClass('m_wrap');
                                    var iwBackground = iwOuter.prev();

                                    iwBackground.css({'display':'none'});
                                    // Remove the background shadow DIV
                                    iwBackground.children(':nth-child(2)').css({'display' : 'none'});
                                    // Remove the white background DIV
                                    iwBackground.children(':nth-child(4)').css({'display' : 'none'});
                                    // Moves the arrow 76px to the left margin 
                                    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                                    var close_btn = iwOuter.next().addClass('close_btn');

                                });
                                googleMap._infoWindow.open( googleMap._gMap, marker);

                            }
                        })(googleMap._gMarker, i));
                        googleMap._arrGMarker[i] = googleMap._gMarker;
                    }
                }

                googleMap._gMap.fitBounds(bounds);
                var listener = google.maps.event.addListener(googleMap._gMap, "idle", function () {
                    if (googleMap._gMap.getZoom() > 15) googleMap._gMap.setZoom(15);
                    google.maps.event.removeListener(listener);
                });
            }
        }
    },
    goToMarker: function (e) {
        var _iNumber = $(this).data().number;
        var browser = navigator.userAgent;
        if(browser.match(/(iPhone|iPod|iPad)/)){
            var link = 'comgooglemaps://?daddr='+googleMap._storeList[_iNumber].address+'&directionsmode=driving';
        } 
        else if(browser.match(/Android/)){
            var link ='google.navigation:q='+googleMap._storeList[_iNumber].address+'&'+googleMap._storeList[_iNumber].lat+','+googleMap._storeList[_iNumber].long;
        }
        var lat = googleMap._storeList[_iNumber].lat;
        var long = googleMap._storeList[_iNumber].lng;
        var bounds = new google.maps.LatLngBounds();
        googleMap._infoWindow.setContent('<div class="wrap_marker">'+
                                            '<h4 class="header">' + googleMap._storeList[i].brand_name +' - '+ googleMap._storeList[i].name + '</h4>' + 
                                            
                                            '<div class="subtitle"><p class="address">' + googleMap._storeList[i].address + '</p></div>'+
                                            '<div class="subtitle"><a target="_blank" href="'+(link ? link : 'https://www.google.com/maps/dir/'+googleMap._storeList[i].address+'')+'"><p class="get_direction">'+direct_text+'</p></a></div>'+
                                        '</div>');
        
        
        google.maps.event.addListener(googleMap._infoWindow, 'domready', function() {

                // Reference to the DIV which receives the contents of the infowindow using jQuery
                var iwOuter = $('.gm-style-iw');

                //get parent div
                iwOuter.parent().addClass('m_wrap');
                var iwBackground = iwOuter.prev();

                iwBackground.css({'display':'none'});
                // Remove the background shadow DIV
                iwBackground.children(':nth-child(2)').css({'display' : 'none'});
                // Remove the white background DIV
                iwBackground.children(':nth-child(4)').css({'display' : 'none'});
                // Moves the arrow 76px to the left margin 
                iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                var close_btn = iwOuter.next().addClass('close_btn');

            });

        googleMap._infoWindow.open(googleMap._gMap, googleMap._arrGMarker[_iNumber]);
      
        googleMap._gMap.setZoom(15);
    
        googleMap._gMap.setCenter(new google.maps.LatLng(lat, long));

        $('body').scrollTop(0,500);
        
    },
    geocodeAddress: function(address) {
        geocoder = new google.maps.Geocoder();
        //In this case it gets the address from an element on the page, but obviously you  could just pass it to the method instead
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            //In this case it creates a marker, but you can get the lat and lng from the location.LatLng
            return results[0].geometry.location;
          } else {
            alert("Geocode was not successful for the following reason: " + status);
          }
        });

    }


}

function GetLocation() {
    var geocoder = new google.maps.Geocoder();
    var address = document.getElementById("txtAddress").value;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            alert("Latitude: " + latitude + "\nLongitude: " + longitude);
        } else {
            alert("Request failed.")
        }
    });
};

function goto (url) {
    window.location = url;
}

</script>
@endsection