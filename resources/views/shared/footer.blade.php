<footer>
  <div class="container">
    <div class="row footer_link">
      <div class="col-md-2">
        <a href="/"><img height="50px" src="/img/logo.png"></a>
      </div>
      <div class="col-md-8">
        <ul class="menu-footer">
          <li><a class="gray-color" href="/about">About Got It</a></li>
          <li><a class="gray-color" href="/terms">Term of Use</a></li>
          <li><a class="gray-color" href="/privacypolicy">Privacy Policy</a></li>
          <li><a class="gray-color" href="/operatingregulation">Operating Regulation</a></li>
        </ul>
      </div>
      <div class="col-md-2 text-right">
        <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=19856" target="_blank"><img height="50px" src="/img/bocongthuong.png"></a>
      </div>
    </div>
  </div>
  <div class="container-fluid footer_info">
    <div class="row">
      <div class="col-md-4 col-md-push-4">
        <div class="row">
          <div class="col-md-6 text-right">
            <p class="hotline pull-right">Hotline: <strong>1900 55 88 20</strong></p>
          </div>
          <div class="col-md-6 text-left">
            <p class="fax pull-left">Fax: <strong>08.3910.0080</strong></p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-md-pull-4 text-left">
        <p>Copyright © DayOne JSC.All right reserved</p>
      </div>
      <div class="col-md-4 col-md-4 text-right">
        <a class="backtop dark-gray-color scroll" href="javascript:void(0)">Back to top</a>
      </div>
    </div>
  </div>
</footer>