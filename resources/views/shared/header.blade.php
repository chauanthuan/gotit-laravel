<nav class="navbar navbar-inverse showMenu">
  <div class="container ">
    <div class="navbar-header">
      <button class="navbar-toggle clicked" data-target=".navbar-collapse" data-toggle="collapse" type="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="logo" href="/"><img alt="Logo" src="/img/logo.png"></a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right mainMenu">
        <li><a href="/" class="">Home</a></li>
        <li><a href="/about-egift">About eGift</a></li>
        <li><a href="/send-egift">Send eGift</a></li>
        <li><a href="/support">Support</a></li>
        <li class="dropdown dropdown-user" dropdown="">
          <a class="dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" dropdown-open="" aria-expanded="false">
            <img alt="" class="img-circle" src="https://lh6.googleusercontent.com/-XLHhiRQHqQg/AAAAAAAAAAI/AAAAAAAAAGo/IHgmkHY8AIE/photo.jpg">
            <span class="username">
              châu nguyen</span>
              <span class="caret-user"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li>
                <a href="/account">
                 Account Settings</a>
               </li>
               <li>
                <a href="/gift-received">
                 eGifts Received
               </a>
             </li>
              <li>
                <a href="/gift-sent">
                 eGifts Sent</a>
               </li>
             <li class="divider">
             </li>
             <li>
              <a href="javascript:void(0)">
                <i class="icon-key"></i> Log Out </a>
              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-lang" dropdown="">
            <a class="dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" dropdown-open="" href="javascript:void(0);">
              <img alt="" class="cricle-lang" src="/img/lang-en.png">
               <span class="caret-lang"></span>
            </a>
            <ul class="dropdown-menu drop_language" role="menu">
              <li><a data-lang="vi" href="javascript:void(0)">
                <img alt="" src="/img/lang-vi.png"> &nbsp;&nbsp;Tiếng Việt</a></li>
              <li><a class="" data-lang="en" href="javascript:void(0)">
                <img alt="" src="/img/lang-en.png"> &nbsp;&nbsp;English</a></li>
            </ul>
          </li>
        </ul>
        <div class="footer_menumb">
            <p> Language: <span class="">VN</span><span class="active">EN</span></p>
        </div>
    </div>
    </div>
  </nav>


  <div aria-hidden="true" aria-labelledby="mySmallModalLabel" bsmodal="" class="modal fade" id="logoutModal" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Log Out</h4>
          </div>
          <div class="modal-body">
            <div class="not_submit">
              <p>Are you sure you want to log out?</p>
              <div class="group_button">
                    <button class="btn btn-default btn_cancel" type="button">Cancel</button>
                    <button class="btn btn-default btn_logout" type="button">Log Out</button>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  <div aria-hidden="true" aria-labelledby="mySmallModalLabel" bsmodal="" class="modal fade" id="loginModal" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="content">
              <button aria-label="Close" class="close pull-right" type="button"></button>
              <h2 class="title">Log In</h2>
              <p class="subtitle">Don’t have an account? <a href="/signup">Create your account</a> now! It takes less than a minute.</p>
              <form action="#" autocomplete="off" class="login-form ng-dirty ng-touched ng-valid" method="post" novalidate="">
                <div class="col-md-12">
                  <div class="row">
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group form-md-floating-label">
                  <input autocomplete="off" id="emailModal" name="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" required="" type="text" class="form-control edited ng-dirty ng-valid ng-touched">
                  <label for="emailModal">Email</label>
                </div>
                <div class="form-group form-md-floating-label">
                  <input autocomplete="password" id="passwordModal" name="password" required="" type="password" class="form-control edited ng-dirty ng-valid ng-touched">
                  <label for="passwordModal">Password</label>
                </div>
                <div class="form-actions">
                  <div class="pull-left">
                    <input id="remember-modal" name="remember" type="checkbox" value="1">
                    <label class="rememberme check" for="remember-modal">Remember me </label>
                  </div>
                  <div class="pull-right forget-password-block">
                    <a class="forget-password" href="/forgotpassword" id="forget-password">Forgot password</a>
                  </div>
                </div>
                <div class="form-actions">
                  <button class="btn btn-submit" type="submit">Log In</button>
                </div>
                <div class="login-options">
                  <h4 class="">Or</h4>
                  <div class="group_button">
                    <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:void(0);">Login with Facebook</a>
                    <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:void(0);">Login with Google</a>
                  </div>
                </div>
              </form>
              </div>
            </div>
        </div>
      </div>
    </div>

<div aria-hidden="true" aria-labelledby="mySmallModalLabel" bsmodal="" class="modal fade" id="enterPassModal" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="overflow:hidden">
        <h4 class="modal-title pull-left">Notification</h4>
      </div>
      <div class="modal-body">
        <p>An account already exists with the same email address but different sign-in credentials. Please enter your password with this email address. System will <span style="color:#ff5f5f">auto redirect after 10s</span>.</p>
        <input autofocus="" class="form-control ng-untouched ng-pristine ng-valid" id="passwordRequiredModal" name="passwordRequired" placeholder="Your password" type="password">
      </div>
    </div>
  </div>
</div>