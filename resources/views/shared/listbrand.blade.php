<section class="brand-section">
  	<div class="container">
    	<div class="row">
	      	<div class="col-md-12 ">
				<div class="box_top tabbable-line">
					<h2 class="pull-left">Our Brands</h2>
					<div class="pull-right dropdown-mobile wrapper-dropdown" id="tab_all_brand" tabindex="1">
						<span>Category<small>All categories</small></span>
						<ul class="nav nav-tabs en">
							<li class="active">
								<a aria-expanded="false" data-id="0" data-toggle="tab" href="#tab_1">
								All categories</a>
							</li>
							<li>
								<a aria-expanded="true" data-id="2" data-toggle="tab" href="#tab_2">
								Fastfood &amp; Café </a>
							</li>
							<li class="">
								<a aria-expanded="false" data-id="1" data-toggle="tab" href="#tab_3">
								Restaurants </a>
							</li>
							<li>
								<a aria-expanded="true" data-id="3" data-toggle="tab" href="#tab_4">
								Fashion &amp; Cosmetic </a>
							</li>
							<li class="">
								<a aria-expanded="false" data-id="7" data-toggle="tab" href="#tab_5">
								Mall &amp; Mart </a>
							</li>

						</ul>
			        </div>
				</div>
				<div class="box-content">
					<div class="tab-content">
						<style type="text/css">
							.tab-pane .list_item{
							    display: grid;
							    grid-template-columns: repeat(auto-fill, minmax(120px, 1fr));
							    grid-gap: 50px;
							    border-bottom: 1px dashed #e3e3e3;
							    padding: 10px 0;
							}
						
							.tab-pane .list_item a img {
							    width: 120px;
							}
						</style>
						<div class="tab-pane active" id="tab_1">
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
						</div>
						<div class="tab-pane " id="tab_2">
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
							
						</div>
						<div class="tab-pane" id="tab_3">
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
						</div>
						<div class="tab-pane" id="tab_4">
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
						</div>
						<div class="tab-pane" id="tab_5">
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/brand/1453994997_xnjWC.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1470128103_0bgUA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png">
								</a>
							</div>
							<div class="list_item" style="display:none">
								<a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1479370699_B6YWt.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1476931339_X80IM.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1468227543_adilc.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png">
								</a><a class="brand_img" href="javascript:void(0)">
									<img src="https://img.gotit.vn/compress/brand/1492658268_29LMD.png">
								</a>
							</div>
						</div>
						<p class="show-more">
							<button class="btn btn-show-more">Show more</button>
						</p>
					</div>
				</div>
			</div>
    	</div>
  	</div>
</section>
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		
		$(".tab-pane .list_item").slice(0, 3).show();
		if ($(".tab-pane.active .list_item:hidden").length != 0) {
			// $(".tab-pane .btn-show-more").text((lang == "vi" ? "Xem thêm "+(countCate - 2)+" thể loại" : "View more "+(countCate - 2)+" categories"));
			$(".tab-pane.active .btn-show-more").show();
		}   
		else{
			$(".tab-pane.active .btn-show-more").hide();
		}

		$('.btn-show-more').click(function (e) {
			e.preventDefault();
           $(".tab-pane.active .list_item").slice(3, 6).slideDown();
	    });
		$('.brand-section .box_top .nav-tabs li').click(function(){
			setTimeout(function(){ 
				$('.tab-pane.active .list_item').slice(0, 3).show();
			}, 100);
		})
	    
	})
</script>
@endsection
