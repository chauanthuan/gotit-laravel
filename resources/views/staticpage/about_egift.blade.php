@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/about_egift.css') !!}">
<link rel="stylesheet" href="{!! asset('css/listbrand.css') !!}">
@endsection
@section('content')
<section class="head-landing">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="head-title">
					<h2>Got It eGift Voucher</h2>
					<p>Redeem at over 75 famous brands in over 1500 locations nationwide</p>
					<a class="btn btn-lg btn-send" href="/send-egift">Send an eGift
						
					</a>
				</div>
				<div class="head-content">
                    <div class="row">

					<div class="col-md-6 first">
                        <div class="box-item box1">
    						<img src="/img/ic-multi-choices.png">
                            
    						<h4>Variety of choices</h4>
    						<p>Choose from a variety of categories including restaurants, café’s, fashion, travel and entertainment.</p>
                        </div>
                        <div class="box-item">
                            <img src="/img/ic-pickup.png">
                            <h4>Redemption Nationwide</h4>
                            <p>Redeem Got It eGift Vouchers instantly 1500 locations nationwide.</p>
                        </div>
					</div>
					<div class="col-md-6 second">
                        <div class="box-item box2">
    						<img src="/img/ic-simple.png">
    						<h4>Easy to use</h4>
    						<p>Simply show your voucher to the store staff to redeem instantly.</p>
                        </div>
                        <div class="box-item box3">
                            <img src="/img/ic-convenience.png">
                            <h4>Convenient storage for your voucher/s</h4>
                            <p>Use immediately or save for another day with an easy keep function.</p>
                        </div>
					</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
                <div class="img1">
                    
                </div>    
            </div>
		</div>
</div>
</section>
@include('shared.listbrand')
<section class="voucher-section">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                </div>

                <div class="col-md-8 voucher-step">

                     <div class="step step1">
                        <img src="/img/ic-gift2x.png">
                             <p>
                                Choose Your Gift
                            </p>                      
                    </div>
                    <div class="step step2">
                        <img src="/img/ic-personalize2x.png">
                            <p>
                                Write Message
                            </p>
                            
                    </div>
                    <div class="step step3">
                        <img src="/img/ic-send2x.png">
                            <p>
                                Send Gift
                            </p>
                            
                    </div>



                </div>
                <div class="col-md-2">
                    
                </div>
            </div>
            <div class="row">
                <div class="voucher-egift">
                    <!---->
                    <ul>
                        <!----><li>
                            <p class="productname">Got It eGift</p>
                            <p>50,000 VND</p>
                            <a class="btn-sendnow" href="/send-egift">Send now</a>
                        </li><li>
                            <p class="productname">Got It eGift</p>
                            <p>100,000 VND</p>
                            <a class="btn-sendnow" href="/send-egift">Send now</a>
                        </li><li>
                            <p class="productname">Got It eGift</p>
                            <p>200,000 VND</p>
                            <a class="btn-sendnow" href="/send-egift">Send now</a>
                        </li><li>
                            <p class="productname">Got It eGift</p>
                            <p>500,000 VND</p>
                            <a class="btn-sendnow" href="/send-egift">Send now</a>
                        </li><li>
                            <p class="productname">Got It eGift</p>
                            <p>1,000,000 VND</p>
                            <a class="btn-sendnow" href="/send-egift">Send now</a>
                        </li>
                        
                        

                    </ul>
                </div>
            </div>
        </div>
    </div>   
</section>
@endsection