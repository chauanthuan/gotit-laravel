@extends('layouts.master')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/about.css') !!}">
@endsection
@section('content')
<section class="section-content">
	<div class="container" id="static_page">
		<div class="row row-no-padding">
			<div class="about_page" style="padding: 10px;margin: 40px 120px 0px 120px;">
				<strong class="title">ABOUT GOT IT</strong>
				<div>
					<p>DAYONE will launch Vietnam’s first premium instant mobile gifting application and website in late 2015.</p>
					<p>DAYONE will launch ‘Got It’ which we believe will quickly become the preferred choice of Vietnamese for simplistic and instant gifting.</p>
					<p>DAYONE isn’t your standard start up company, with a dynamic and very experienced team each with a proven history in understanding the needs of partners and consumers and with a relentless drive for success</p>
					<p>DAYONE brings honesty and integrity and believes in long term win/win partnerships which will be cherished and grown over many years</p>
				</div>

			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		var w_width = $(window).width();
	    var w_height = $(window).height();
	    var footer_height = $('footer').height();
	    $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});

	    $(window).resize(function(){
            var w_width = $(window).width();
            var w_height = $(window).height();
            var footer_height = $('footer').height();
            $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});
        })
	})
</script>
@endsection