@extends('layouts.master')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/operate.css') !!}">
@endsection
@section('content')
<section class="section-content">
	<div class="container" id="static_page">
	 	<div class="row row-no-padding">
	 		<div class="about_page" style="padding: 10px;margin: 40px 120px 0px 120px;font-family: Tahoma;">
	 			<div>
	 				<div>
	 					<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
	 						<tbody>
	 							<tr>
	 								<td style="width:30%">
	 									<p style="text-align: center"><strong>CÔNG TY CỔ PHẦN DAYONE</strong></p>

	 									<hr style="border-color: #000000; width: 80%; margin: 5px auto;">

	 									<p style="margin-left: 30px;">Số:&nbsp; 01/2015&nbsp;&nbsp;</p>
	 								</td>
	 								<td style="width:70%; text-align: center;">
	 									<p><strong>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</strong></p>

	 									<p><strong>Độc lập – Tự do – Hạnh phúc</strong></p>

	 									<hr style="border-color: #000000; width: 230px; margin: 5px auto;">

	 									<p class="about_page_space">&nbsp;</p>
	 								</td>
	 							</tr>
	 							<tr>
	 								<td style="height:25px; width:263px">
	 									<p>&nbsp;</p>
	 								</td>
	 								<td style="height:25px; width:383px; text-align: center">
	 									<p><i>TP Hồ Chí Minh, ngày 27 tháng 10 năm 2015</i></p>
	 								</td>
	 							</tr>
	 						</tbody>
	 					</table>

	 					<div style="clear:both;">&nbsp;</div>

	 					<p>&nbsp;</p>

	 					<p style="text-align: center;"><strong>QUYẾT ĐỊNH</strong></p>

	 					<p style="text-align: center;"><i>(Vv Ban hành Quy chế hoạt động Sàn Giao dịch Thương mại Điện tử)</i></p>

	 					<p>&nbsp;</p>

	 					<p style="text-align: center;"><strong>HỘI ĐỒNG QUẢN TRỊ</strong></p>

	 					<p>&nbsp;</p>

	 					<ul>
	 						<li> &nbsp;&nbsp;Căn cứ quyền hạn và nhiệm vụ của Hội Đồng Quản Trị theo Điều lệ Công ty Cổ phần DAYONE.</li>
	 						<li> &nbsp;&nbsp;Căn cứ tình hình hoạt động của Công ty.</li>
	 					</ul>

	 					<p style="margin-left:18.0pt">&nbsp;</p>

	 					<p style="margin-left:18.0pt;text-align: center;"><strong>QUYẾT ĐỊNH</strong></p>

	 					<p style="margin-left:18.0pt">&nbsp;</p>

	 					<p><strong><u>Điều 1:</u></strong></p>
	 					<p>Ban hành Quy chế hoạt động Sàn Giao dịch Thương mại Điện tử và niêm yết Quy chế hoạt động Sàn Giao dịch Thương mại Điện tử công khai trên trang chủ của Website: www.gotit.vn.</p>
	 					<p><strong><u>Điều 2:</u></strong></p>
	 					<p>Giao cho người đại diện pháp luật Công ty và các thành viên Ban Giám đốc tổ chức nhân sự quản lý hoạt động của Sàn giao dịch và triển khai thi hành Quyết Định này.</p>
	 					<p><strong><u>Điều 3:</u></strong></p>
	 					<p>Quyết định có hiệu lực kể từ ngày ký.</p>
	 					<p style="text-align: right; margin-right: 110px;"><strong>THAY MẶT HỘI ĐỒNG QUẢN TRỊ</strong></p>

	 					<p style="text-align: right; margin-right: 110px;"><strong>CHỦ TỊCH HỘI ĐỒNG QUẢN TRỊ</strong></p>

	 					<p>&nbsp;</p>

	 					<p>&nbsp;</p>

	 					<p style="text-align: right; margin-right: 125px;"><strong> ______________________</strong></p>

	 					<p style="text-align: right; margin-right: 140px;"><strong>NGUYỄN THỊ HẢI MINH</strong></p>

	 					<p style="margin-left:18.0pt">&nbsp;</p>

	 					<p>&nbsp;</p>
	 				</div>

	 				<p>&nbsp;</p>

	 				<p style="text-align: center;"><strong>QUY CHẾ HOẠT ĐỘNG</strong></p>

	 				<p style="text-align: center;"><strong>SÀN GIAO DỊCH THƯƠNG MẠI ĐIỆN TỬ DAYONE</strong></p>

	 				<p style="text-align: center;"><i>(Ban hành kèm theo Quyết định số  01/2015 ngày 27 tháng 10  năm 2015)</i></p>
	 				<ol type="I">
	 					<li><b>Nguyên tắc chung</b>
	 						<ol>
	 							<li>Sàn giao dịch thương mại điện tử DAYONE do Công ty Cổ phần DAYONE (địa chỉ trang website: <a href="http://www.gotit.vn">www.gotit.vn</a>) (sau đây gọi tắt là “Sàn giao dịch”) thực hiện hoạt động và vận hành, cung cấp các dịch vụ trực tuyến về quà tặng và các dịch vụ thương mại điện tử được phép khác. Bản Quy chế này áp dụng cho các thành viên đăng ký sử dụng, tham gia các chương trình được tổ chức thực hiện trên Sàn giao dịch.</li>

	 							<li>Sàn giao dịch được triển khai giới thiệu các sản phẩm, dịch vụ và thực hiện các chương trình bán hàng, chương trình khuyến mại cho các sản phẩm, dịch vụ theo uỷ quyền của các thương nhân, tổ chức, cá nhân có hoạt động thương mại hợp pháp (sau đây gọi chung là “thương nhân” hoặc “nhà bán hàng”) trên cơ sở Hợp đồng, Thoả thuận với Công ty Cổ phần DAYONE và không trái với quy định của pháp luật hiện hành.</li>
	 							<li>Khách hàng tham gia Sàn giao dịch là các cá nhân có đầy đủ năng lực hành vi dân sự và phải đăng ký kê khai ban đầu về các thông tin cá nhân bắt buộc theo yêu cầu và được Ban quản lý Sàn giao dịch chính thức công nhận cho phép sử dụng dịch vụ do Sàn giao dịch cung cấp.
	 								<ul>
	 									<p>Khi đăng ký là thành viên của gotit.vn, Thành viên hiểu rằng:</p>
	 									<p>+ Thành viên có thể tạo một tài khoản cá nhân của mình để sử dụng.</p>
	 									<p>+ Thành viên có thể mua hàng hóa, dịch vụ theo đúng giá và quy chuẩn theo đúng cam kết của thương nhân hợp pháp đã công bố trên sàn</p>
	 									<p>+ Thành viên có thể mua hàng trên trang web để sử dụng làm quà tặng cho người khác hoặc sử dụng cho chính mình.</p>
	 									<p>+ Thành viên phải bảo vệ mật khẩu của mình và giám sát việc sử dụng các tài khoản của mình, Thành viên hiểu và đồng ý rằng Thành viên chịu trách nhiệm mọi thông tin cá nhân có liên quan kể cả việc sử dụng tài khoản của mình, và Thành viên phải chịu hoàn toàn trách nhiệm đối với bất cứ ai được Thành viên cho phép truy cập vào nó.</p>
	 									<p>+ Thành viên đại diện cho rằng bạn có quyền cung cấp bất kỳ và tất cả các thông tin Thành viên gửi đến trang web, thông tin đó chỉ là về bản thân, và rằng tất cả các thông tin như vậy là chính xác, đúng sự thật, và đầy đủ</p>
	 								</ul>
	 							</li>
	 							<li>Sản phẩm, dịch vụ tham gia giao dịch trên Sàn giao dịch phải được phép kinh doanh, lưu hành và đáp ứng đầy đủ các điều kiện của pháp luật có liên quan và không thuộc các trường hợp cấm kinh doanh, cấm quảng cáo theo quy định của pháp luật.</li>
	 							<li>Mọi hoạt động mua bán hàng hóa, cung cấp dịch vụ trên Sàn giao dịch phải được thực hiện công khai, minh bạch, đảm bảo quyền lợi của người tiêu dùng.</li>
	 							<li>Tất cả các nội dung trong Quy định này phải tuân thủ theo hệ thống pháp luật hiện hành của Việt Nam. Khách hàng, các Thương nhân/Nhà bán hàng khi tham gia vào Sàn giao dịch phải tự tìm hiểu trách nhiệm pháp lý của mình đối với luật pháp hiện hành của Việt Nam và cam kết thực hiện đúng những nội dung trong Quy chế của Sàn giao dịch.</li>
	 						</ol>
	 					</li>
	 					<li><b>Quy trình giao dịch</b>
	 						<p>Ban quản lý Sàn giao dịch sẽ giới thiệu đến Khách hàng các sản phẩm, dịch vụ, chương trình bán hàng, chương trình khuyến mại của các thương nhân khác có nhu cầu khuyến mại cho các sản phẩm hàng hóa do thương nhân đó đang trực tiếp kinh doanh và đưa ra mức giá, các phương thức thanh toán, Khách hàng có thể tham khảo và lựa chọn mua để sử dụng các sản phẩm, dịch vụ được khuyến mại của các thương nhân thực hiện, nếu xét thấy phù hợp với nhu cầu cá nhân.</p>
	 						<ol>
	 							<li>Quy trình dành cho Khách hàng:
	 								<p>Khi có nhu cầu mua hàng, sử dụng dịch vụ trên Sàn giao dịch, Khách hàng sẽ thực hiện theo các bước sau đây:</p>
	 								<ul>
	 									<p>- <b>Bước 1:</b> Tìm kiếm, tham khảo thông tin sản phẩm, dịch vụ trên Sàn giao dịch mà Khách hàng đang quan tâm, sau đó, Khách hàng có thể lựa chọn sản phẩm, dịch vụ thích hợp do từng Thương nhân/Nhà bán hàng cung cấp.</p>
	 									<p>- <b>Bước 2:</b> Dựa trên thông tin tham khảo, Khách hàng đưa ra quyết định đặt hàng trực tuyến bằng cách nhấn vào nút “<b>Gởi quà</b>” hoặc “<b>Thêm vào giỏ quà</b>”, nếu Khách hàng chưa đăng nhập thì hệ thống sẽ yêu cầu khách hàng đăng nhập vào tài khoản giao dịch tại Sàn Giao dịch và lựa chọn phương thức thanh toán và thực hiện thanh toán trực tuyến</p>
	 									<p>- <b>Bước 3:</b> Khách hàng chọn số lượng hàng cần mua</p>
	 									<p>- <b>Bước 4:</b> Khách hàng nhập lời nhắn cho món quà tặng để gửi đến người nhận</p>
	 									<p>- <b>Bước 5:</b> Khách hàng chọn hình thức thanh toán mong muốn, bao gồm thanh toán bằng thẻ thanh toán quốc tế (Visa, Master card..) thông qua công thanh toán trực tuyến hoặc thanh toán bằng tài khoản ví điện tử MoMo, sau đó thực hiện thanh toán trực tuyến </p>
	 									<p>- <b>Bước 6:</b> Khách hàng chọn hình thức gửi Thẻ quà tặng điện tử mà người nhận quà tặng sẽ nhận, bao gồm email, sms, ứng dụng OTT hoặc mạng xã hội </p>
	 									<p>- <b>Bước 7:</b> Người nhận Thẻ quà tặng điện tử sẽ sử dụng Thẻ quà tặng điện tử tại các Địa điểm sử dụng Thẻ quà tặng điện tử được ghi nhận trên Thẻ hoặc niêm yết trên Sàn Giao Dịch để nhận sản phẩm, sử dụng dịch vụ theo các điều kiện sử dụng và hạn sử dụng cụ thể trên Thẻ quà tặng điện tử.</p>

	 								</ul>
	 							</li>
	 							<li>Quy trình dành cho các Thương nhân/Nhà Bán hàng:
	 								<ul>
	 									<p>- <b>Bước 1:</b> Thương nhân/Nhà Bán hàng sẽ giao kết hợp đồng với Công ty cổ phần DAYONE liên quan đến việc cung ứng sản phẩm, dịch vụ trên Sàn Giao Dịch. Theo đó, các sản phẩm, dịch vụ được đưa lên Sàn Giao Dịch sẽ tuân theo thoả thuận bằng Hợp đồng giữa các bên.</p>
	 									<p>- <b>Bước 2:</b> Thương nhân/Nhà Bán hàng được cung cấp tài khoản trên Trang Quản Trị để Thương nhân/Nhà Bán hàng thực hiện việc xác nhận tính hợp lệ của Thẻ Quà Tặng Điện Tử trước khi thực hiện các giao dịch liên quan đến việc sử dụng Thẻ Quà Tặng Điện Tử của Khách Hàng và quản lý, theo dõi  các dữ liệu bán hàng và các thông tin khác.</p>
	 									<p>- <b>Bước 3:</b> Thương nhân/Nhà Bán hàng có trách nhiệm chuẩn bị sản phẩm, dịch vụ sẵn sàng cho tất cả Thẻ Quà Tặng Điện tử đã phát hành cho Khách Hàng và cam kết sẽ cung cấp Sản phẩm, Dịch vụ tương ứng với giá trị và nội dung của Thẻ Quà Tặng Điện Tử được cấp cho các Khách hàng của Sàn Giao dịch.</p>

	 								</ul>
	 							</li>
	 							<li>Quy trình giao nhận vận chuyển:
	 								<p>Khách hàng sau khi thanh toán trực tuyến sẽ chọn hình thức gửi Thẻ quà tặng điện tử mà người nhận quà tặng sẽ nhận, bao gồm email, SMS, ứng dụng OTT hoặc mạng xã hội. Sau đó người nhận được Thẻ quà tặng điện tử sẽ đem đến các Địa Điểm sử dụng Thẻ Quà Tặng Điện Tử để đổi lấy sản phẩm, sử dụng dịch vụ. Do vậy, về nguyên tắc Sàn Giao Dịch không phải thực hiện vận chuyển sản phẩm đến Khách hàng. </p>
	 							</li>
	 							<li>Chính sách đổi trả sản phẩm/dịch vụ:
	 								<p>Khách hàng sau khi nhận sản phẩm, dịch vụ từ các Địa Điểm sử dụng thẻ quà tặng điện tự có thể đổi trả sản phẩm, dịch vụ theo quy chế, quy định của Thương Nhân/ Nhà Bán Hàng cung cấp sản phẩm, dịch vụ đó. Thẻ Quà Tặng Điện Tử do Sàn Giao Dịch phát hành cho khách hàng không có giá trị quy đổi ra tiền mặt.</p>
	 							</li>
	 							<li>Chính sách bảo hành sản phẩm/dịch vụ:
	 								<p>Khách hàng có thể được bảo hành sản phẩm, dịch vụ theo quy chế, quy định của Thương Nhân/ Nhà Bán Hàng cung cấp sản phẩm, dịch vụ đó..</p>
	 							</li>

	 						</ol>
	 					</li>
	 					<li><b>Đảm bảo an toàn giao dịch</b>
	 						<ol>
	 							<li>Ban quản lý Sàn Giao Dịch sử dụng các dịch vụ để bảo vệ thông tin và việc thanh toán của các Khách hàng và Thương nhân/Nhà Bán hàng, cụ thể như sau:
	 								<ol>
	 									<li>Sử dụng phần mềm Secure Sockets Layer (SSL) để bảo vệ thông tin của khách hàng trong quá trình chuyển dữ liệu bằng cách mã hóa thông tin Khách hàng nhập vào.</li>
	 									<li>Khi Khách hàng truy cập gotit.vn, chúng tôi sẽ đặt một số File dữ liệu nhỏ gọi là Cookies lên đĩa cứng hoặc bộ nhớ máy tính của Khách hàng. Một trong số những Cookies này có thể tồn tại lâu để thuận tiện cho bạn trong quá trình sử dụng, ví dụ như: lưu Email của bạn trong trang đăng nhập để bạn không phải nhập lại v.v…Chúng tôi sẽ mã hóa các File Cookies để bảo mật, bạn có thể cấm Cookies trên trình duyệt của mình nhưng điều này có thể ảnh hưởng đến quá trình sử dụng gotit.vn của bạn.</li>
	 									<li> Đối với hình thức thanh toán sử dụng Cổng thanh toán trực tuyến, chúng tôi sử dụng Cổng thanh toán trực tuyến Sacombank ePAY. Với Sacombank ePAY,  thông tin thẻ và chủ thẻ được mã hóa và đảm bảo bí mật bằng các công nghệ bảo mật tiên tiến hàng đầu thế giới, bao gồm:
	 										<img src="/img/epay.png" style="width:400px;display:block;margin:0 auto;">
	 									</li>
	 									<li>Đối với hình thức thanh toán sử dụng ví điện tử, chúng tôi sử dụng ứng dụng MoMo của Công ty cổ phần Dịch Vụ Di Động Trực Tuyến (M_Service). Ứng dụng MoMo được thiết kế bảo mật 256 bit theo chuẩn ngân hàng và chứng nhận bảo mật Verisign để đảm bảo an toàn cho các giao dịch</li>
	 									<li>Chúng tôi chỉ cung cấp 4 chữ số cuối của số thẻ tín dụng của Khách hàng khi xác nhận đơn hàng. Tất nhiên, chúng tôi sẽ cung cấp đầy đủ số thẻ tín dụng của Khách hàng cho công ty thẻ tín dụng phù hợp trong quá trình xử lý đơn hàng.</li>
	 								</ol>
	 							</li>
	 							<li>Để đảm bảo các giao dịch được tiến hành thành công, hạn chế tối đa rủi ro có thể phát sinh, yêu cầu các Khách hàng tham gia Sàn giao dịch lưu ý và tuân thủ các nội dung cam kết như sau:
	 								<ol>
	 									<li>Bảo vệ là việc hỗ trợ cho Khách hàng, hệ thống của Sàn giao dịch được thiết kế tích hợp các chương trình bảo mật để Khách hàng thực hiện đặt hàng, thanh toán, và thoát khỏi chương trình một cách an toàn.</li>
	 									<li>Khách hàng không nên đưa thông tin chi tiết về việc thanh toán với bất kỳ ai bằng e-mail, chúng tôi không chịu trách nhiệm về những mất mát Khách hàng có thể gánh chịu trong việc trao đổi thông tin của Khách hàng qua internet hoặc e-mail.</li>
	 									<li>Khách hàng tuyệt đối không sử dụng bất kỳ chương trình, công cụ hay hình thức nào khác để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ liệu. Nghiêm cấm việc phát tán, truyền bá hay cổ vũ cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm của hệ thống website. Mọi vi phạm sẽ bị xử lý theo Quy chế và quy định của pháp luật.</li>
	 									<li>Mọi thông tin giao dịch được bảo mật, trừ trường hợp buộc phải cung cấp khi Cơ quan pháp luật yêu cầu.</li>
	 								</ol>
	 							</li>

	 						</ol>
	 					</li>
	 					<li><b>Chính sách bảo vệ thông tin cá nhân Khách hàng</b>
	 						<ol>
	 							<li>Mục đích thu thập thông tin cá nhân
	 								<p>Việc thu thập dữ liệu chủ yếu trên Sàn giao dịch bao gồm: email, điện thoại, tên đăng nhập, mật khẩu đăng nhập, địa chỉ của Khách hàng. Đây là các thông tin mà Ban Quản Lý Sàn Giao dịch cần Khách hàng cung cấp bắt buộc khi đăng ký sử dụng dịch vụ và để Ban Quản Lý Sàn Giao dịch liên hệ xác nhận khi khách hàng đăng ký sử dụng dịch vụ trên website nhằm đảm bảo quyền lợi cho cho người tiêu dùng.</p>
	 								<p>Sàn giao dịch cũng lưu trữ bất kỳ thông tin nào bạn nhập trên website hoặc gửi đến gotit.vn. Những thông tin đó sẽ được sử dụng cho mục đích phản hồi yêu cầu của khách hàng, đưa ra những gợi ý&lrm; phù hợp cho từng khách hàng khi mua sắm tại gotit.vn, nâng cao chất lượng hàng hóa dịch vụ và liên lạc với khách hàng khi cần.</p>
	 								<p>Ngoài ra, các thông tin giao dịch gồm: lịch sử mua hàng, giá trị giao dịch, phương thức nhận Thẻ quà tặng điện tử và thanh toán cũng được Sàn giao dịch lưu trữ nhằm giải quyết những vấn đề có thể phát sinh về sau.</p>
	 								<p>Các Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình. Ngoài ra, Khách hàng có trách nhiệm thông báo kịp thời cho Sàn giao dịch về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.</p>
	 							</li>
	 							<li>Phạm vi sử dụng thông tin
	 								<p>Sàn giao dịch sử dụng thông tin Khách hàng cung cấp để:</p>
	 								<p>• &nbsp;&nbsp;   Cung cấp các dịch vụ đến Khách hàng.</p>
	 								<p>• &nbsp;&nbsp;   Gửi các thông báo về các hoạt động trao đổi thông tin giữa Khách hàng và Sàn giao dịch.</p>
	 								<p>• &nbsp;&nbsp;   Ngăn ngừa các hoạt động phá hủy tài khoản người dùng của Khách hàng hoặc các hoạt động giả mạo Khách hàng.</p>
	 								<p>• &nbsp;&nbsp;   Liên lạc và giải quyết với Khách hàng trong những trường hợp đặc biệt.</p>
	 								<p>• &nbsp;&nbsp;   Nâng cao chất lượng dịch vụ khách hàng.</p>
	 								<p>• &nbsp;&nbsp;   Giải quyết các vấn đề, tranh chấp phát sinh liên quan đến việc sử dụng website.</p>
	 								<p>• &nbsp;&nbsp;   Ngăn chặn những hoạt động vi phạm pháp luật Việt Nam.</p>
	 								<p>• &nbsp;&nbsp;   Không sử dụng thông tin cá nhân của Khách hàng ngoài mục đích xác nhận và liên hệ có liên quan đến giao dịch tại Sàn giao dịch.</p>
	 								<p>• &nbsp;&nbsp;   Trong trường hợp có yêu cầu của pháp luật: Sàn giao dịch sẽ hợp tác cung cấp thông tin cá nhân Khách hàng khi có yêu cầu từ cơ quan tư pháp bao gồm: Viện kiểm sát, tòa án, cơ quan công an điều tra liên quan đến hành vi vi phạm pháp luật nào đó của Khách hàng, Thương Nhân tham gia Sàn Giao dịch. Ngoài ra, không ai có quyền xâm phạm vào thông tin cá nhân của Khách hàng.</p>

	 							</li>
	 							<li>Thời gian lưu trữ thông tin
	 								<p>Dữ liệu cá nhân của Khách hàng sẽ được lưu trữ cho đến khi có yêu cầu hủy bỏ hoặc tự Khách hàng đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp thông tin cá nhân Khách hàng sẽ được bảo mật trên máy chủ của Sàn Giao dịch. </p>
	 							</li>
	 							<li>Địa chỉ của đơn vị thu thập và quản lý thông tin, bao gồm cách thức liên lạc để người tiêu dùng, Khách hàng có thể hỏi về hoạt động thu thập, xử lý thông tin liên quan đến cá nhân mình
	 								<p>Địa chỉ của đơn vị thu thập thông tin:</p>
	 								<p>&nbsp;&nbsp;<b>CÔNG TY CỔ PHẦN DAYONE</b></p>
	 								<p>&nbsp;&nbsp;Địa chỉ: Lầu 6, Toà nhà Khải Hoàn, 1 Bis Nguyễn Văn Thủ, P.Đa Kao, Q.1, TP.HCM</p>
	 								<p>&nbsp;&nbsp;Điện thoại: 08  3910 0079</p>
	 								<p>&nbsp;&nbsp;Email:  support@gotit.vn </p>

	 							</li>
	 							<li>Phương thức và công cụ để khách hàng, người tiêu dùng tiếp cận và chỉnh sửa dữ liệu cá nhân của mình trên hệ thống thương mại điện tử của đơn vị thu thập thông tin.
	 								<p>• &nbsp;&nbsp;   Khách hàng có quyền tự kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ thông tin cá nhân của mình bằng cách đăng nhập vào tài khoản và chỉnh sửa thông tin cá nhân.</p>
	 								<p>• &nbsp;&nbsp;   Thành viên có quyền gửi khiếu nại về việc lộ thông tin cá nhân cho bên thứ 3 đến Ban quản lý của Sàn giao dịch. Khi tiếp nhận những phản hồi này, Ban quản lý sẽ xác nhận lại thông tin, phải có trách nhiệm trả lời lý do và hướng dẫn Khách hàng khôi phục và bảo mật lại thông tin.</p>
	 								<p>• &nbsp;&nbsp;   Email tiếp nhận thông tin khiếu nại: [support@gotit.vn]</p>
	 							</li>
	 							<li>Bảo mật thông tin khách hàng
	 								<p>• &nbsp;&nbsp;    gotit.vn sử dụng phần mềm Secure Sockets Layer (SSL) để bảo vệ thông tin của khách hàng trong quá trình chuyển dữ liệu bằng cách mã hóa thông tin bạn nhập vào.</p>
	 								<p>• &nbsp;&nbsp;   Khách hàng có trách nhiệm tự bảo vệ mình trước sự tiếp cận thông tin về password khi dùng chung máy tính với nhiều người. Khi đó, Khách hàng phải chắc chắn đã thoát khỏi tài khoản sau khi sử dụng dịch vụ của gotit.vn.</p>
	 								<p>• &nbsp;&nbsp;   Gotit.vn cam kết không cố &lrm;&lrm;ý tiết lộ thông tin khách hàng, không bán hoặc chia sẻ thông tin khách hàng của gotit.vn vì mục đích thương mại vi phạm những cam kết giữa gotit.vn với Khách hàng.</p>
	 								<p>• &nbsp;&nbsp;   Chính sách bảo mật thông tin khách hàng của gotit.vn chỉ được áp dụng tại <a href="http://www.gotit.vn/">http://www.gotit.vn/</a>. Nó không bao gồm hoặc liên quan đến các bên thứ ba khác đặt quảng cáo hoặc có liên kết tại gotit.vn. Khách hàng nên tham khảo và phân biệt rõ sự khác biệt trong chính sách bảo mật thông tin khách hàng của những website này</p>
	 								<p></p>
	 								<p></p>
	 							</li>
	 						</ol>
	 					</li>
	 					<li><b>Cơ chế tiếp nhận và giải quyết khiếu nại</b>
	 						<p>Khi tiếp nhận thông tin khiếu nại, Công ty Cổ phần DAYONE luôn đề cao giải pháp thương lượng, hòa giải giữa các bên nhằm duy trì sự tin cậy của Khách hàng vào chất lượng dịch vụ của Công ty. Quy trình tiếp nhận và giải quyết khiếu nại sẽ tiến hành theo các bước như sau:</p>
	 						<ol>
	 							<li>Bước 1: Khách hàng khiếu nại về sử dụng thẻ quà tặng điện tử qua email: support@gotit.vn  hoặc tổng đài (08) 3910 0079  (từ thứ 2 đến thứ 6, từ 9 giờ sáng đên 6 giờ chiều).</li>
	 							<li>Bước 2: Bộ phận Chăm Sóc Khách Hàng của Công ty sẽ tiếp nhận các khiếu nại của Khách hàng hoặc Bộ phận Kinh doanh sẽ tiếp nhận các khiếu nại của Thương nhân/Nhà Bán hàng. Các Bộ phận này chủ động giải quyết nhanh chóng và trả lời ngay kết quả giải quyết các khiếu nại trên cơ sở các Chính sách mà Công ty đã công bố.</li>
	 							<li>Bước 3: Trong trường hợp phức tạp hoặc không được quy định tại các chính sách mà Công ty đã công bố thì Bộ phận Chăm Sóc Khách Hàng/ Bộ phận Kinh doanh sẽ cam kết thời hạn phản hồi cho Khách hàng/Nhà Bán hàng không quá 7 ngày.</li>
	 							<li>Bước 4: Chuyển kết quả giải quyết khiếu nại cho các bộ phận có liên quan để thực hiện (Bộ phận Kế toán – Tài chính, Bộ phận Giao hàng…) và gọi điện xác nhận với Khách hàng/Nhà Bán hàng về kết quả khiếu nại đã được giải quyết.
	 								<p>Khi phát hiện thông tin cá nhân của mình bị sử dụng sai mục đích hoặc phạm vi, Khách hàng/Nhà Bán hàng có quyền gởi email khiếu nại đến Địa chỉ liên lạc của Công ty Cổ phần DAYONE như nêu trên với các thông tin, chứng cứ liên quan tới việc này. Công ty cam kết sẽ phản hồi ngay lập tức trong vòng 24 tiếng để cùng Người dùng thống nhất phương án giải quyết.</p>
	 							</li>

	 						</ol>
	 					</li>
	 					<li><b>Bảo vệ quyền lợi người tiêu dùng</b>
	 						<p>Khách hàng khi truy cập vào trang website www.gotit.vn đều biết và hiểu rằng: việc Khách hàng mua hàng hóa, dịch vụ và tham gia các khuyến mại trên Sàn giao dịch đều do các thương nhân có nhu cầu bán hàng hoặc thực hiện khuyến mại thực hiện thông qua dịch vụ của Công ty Cổ phần DAYONE, Ban quản lý Sàn Giao Dịch chỉ thực hiện giới thiệu sản phẩm, dịch vụ quảng bá các chương trình khuyến mại cho các hàng hóa, dịch vụ đến các Khách hàng theo ủy quyền của các thương nhân khác, nên các thương nhân thực hiện khuyến mại sẽ trực tiếp chịu mọi trách nhiệm có liên quan đến chất lượng hàng hóa, dịch vụ khuyến mại theo đúng cam kết đã công bố trên sàn giao dịch.</p>
	 						<p>Khách hàng có quyền gửi khiếu nại trực tiếp đến thương nhân cung cấp hàng hoá/dịch vụ mà không cần thực hiện khiếu nại lần đầu với DAYONE trong các trường hợp sản phẩm, dịch vụ do các Thương nhân thực hiện cung cấp không đảm bảo chất lượng như các thông tin đã công bố.</p>
	 						<p>Ban quản lý Sàn giao dịch sẽ là cầu nối để yêu cầu bồi thường cho Khách hàng khi sử dụng các sản phẩm, dịch vụ không đạt tiêu chuẩn chất lượng hoặc không đúng như mô tả hàng hoá đã cam kết, nhằm đảm bảo quyền lợi hợp pháp cho người tiêu dùng trong các giao dịch gây ảnh hưởng đến lợi ích người tiêu dùng.</p>
	 					</li>
	 					<li><b>Quản lý thông tin xấu</b>
	 						<p>Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật khẩu của mình. Khách hàng có trách nhiệm thông báo kịp thời cho Sàn giao dịch về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.</p>
	 						<p>Khách hàng không sử dụng dịch vụ của Sàn giao dịch vào những mục đích bất hợp pháp, không hợp lý, lừa đảo, đe doạ, thăm dò thông tin bất hợp pháp, phá hoại, tạo ra và phát tán virus gây hư hại tới hệ thống, cấu hình, truyền tải thông tin của Sàn giao dịch hay sử dụng dịch vụ của mình vào mục đích đầu cơ, lũng đoạn thị trường tạo những đơn đặt hàng, chào hàng giả, kể cả phục vụ cho việc phán đoán nhu cầu thị trường. Trong trường hợp vi phạm thì Khách hàng phải chịu trách nhiệm về các hành vi của mình trước pháp luật.</p>
	 						<p>Khách hàng không được thay đổi, chỉnh sửa, gán gép, copy, truyền bá, phân phối, cung cấp và tạo những công cụ tương tự của dịch vụ do Sàn giao dịch cung cấp cho một bên thứ ba nếu không được sự đồng ý của Sàn giao dịch trong bản Quy chế này.</p>
	 						<p>Khách hàng không được hành động gây mất uy tín của Sàn giao dịch dưới mọi hình thức như gây mất đoàn kết giữa các Khách hàng bằng cách sử dụng tên đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên truyền, phổ biến những thông tin không có lợi cho uy tín của Sàn giao dịch.</p>
	 						<p><b>Cơ chế rà soát, kiểm soát thông tin về sản phẩm/dịch vụ của Ban quản lý Sàn giao dịch đối với sản phẩm/dịch vụ đăng tải trên website:</b></p>
	 						<p>•    Ban quản lý Sàn giao dịch sẽ toàn quyền loại bỏ các sản phẩm của Thương nhân nếu sản phẩm bán vi phạm quy chế đăng tin. Các sản phẩm, dịch vụ không phù hợp với chuyên mục quy định sẽ bị xóa hoặc Sàn giao dịch chuyển sang chuyên mục khác cho là hợp lý.</p>
	 						<p>•    Ban quản lý Sàn giao dịch giữ quyền quyết định về việc lưu giữ hay loại bỏ sản phẩm đã đăng trên trang website mà không cần báo trước.</p>
	 						<p>•    Ban quản lý Sàn giao dịch sẽ kiểm soát việc đưa hình ảnh, sản phẩm, dịch vụ lên Sàn Giao Dịch nhằm tuân thủ nghiêm ngặt Luật Quảng Cáo và các quy định pháp luật khác có liên quan.</p>

	 					</li>
	 					<li><b>Trách nhiệm trong trường hợp phát sinh lỗi kỹ thuật</b>
	 						<p>Sàn giao dịch cam kết nỗ lực đảm bảo sự an toàn và ổn định của toàn bộ hệ thống kỹ thuật. Tuy nhiên, trong trường hợp xảy ra sự cố do lỗi của Sàn Giao Dịch, Ban Quản lý Sàn Giao Dịch sẽ ngay lập tức áp dụng các biện pháp để đảm bảo quyền lợi cho Khách hàng.</p>
	 						<p>Khi thực hiện các giao dịch trên Sàn Giao Dịch, bắt buộc các Khách hàng phải thực hiện đúng theo các quy trình hướng dẫn.</p>
	 						<p>Ban quản lý Sàn giao dịch cam kết cung cấp chất lượng dịch vụ tốt nhất cho các Khách hàng tham gia giao dịch. Trường hợp phát sinh lỗi kỹ thuật, lỗi phần mềm hoặc các lỗi khách quan khác dẫn đến Khách hàng không thể tham gia giao dịch được thì các Khách hàng thông báo cho Ban quản lý Sàn giao dịch qua địa chỉ liên hệ của Công ty Cổ phần DAYONE, chúng tôi sẽ khắc phục lỗi trong thời gian sớm nhất, tạo điều kiện cho các Khách hàng tham gia Sàn giao dịch.</p>
	 						<p>Tuy nhiên, Ban quản lý Sàn giao dịch sẽ không chịu trách nhiệm giải quyết trong trường hợp thông báo của các Khách hàng không đến được Ban quản lý, phát sinh từ lỗi kỹ thuật, lỗi đường truyền, phần mềm hoặc các lỗi khác không do Ban quản lý gây ra.</p>

	 					</li>
	 					<li><b>Quyền và nghĩa vụ của Ban quản lý Sàn giao dịch</b>
	 						<ol>
	 							<li><b>Quyền của Ban quản lý Sàn giao dịch</b>
	 								<p>• &nbsp;&nbsp;Ban hành các thủ tục và các điều kiện bắt buộc áp dụng cho Khách hàng, người dùng tham gia Sàn giao dịch.</p>
	 								<p>•  &nbsp;&nbsp;  Trong trường hợp có cơ sở để chứng minh Khách hàng/Nhà Bán hàng cung cấp thông tin cho Sàn giao dịch không chính xác, sai lệch, không đầy đủ hoặc vi phạm pháp luật hay thuần phong mỹ tục Việt Nam thì Sàn giao dịch có quyền từ chối, tạm ngừng hoặc chấm dứt quyền sử dụng dịch vụ của Khách hàng/Nhà Bán hàng.</p>
	 								<p>• &nbsp;&nbsp;   Ban quản lý sàn giao dịch có thể chấm dứt quyền Khách hàng và quyền sử dụng một hoặc tất cả các dịch vụ của Khách hàng và sẽ thông báo cho Khách hàng trong thời hạn ít nhất là một (01) tháng trong trường hợp Khách hàng vi phạm các Quy chế của Sàn giao dịch hoặc có những hành vi ảnh hưởng đến hoạt động kinh doanh trên Sàn giao dịch.</p>
	 								<p>• &nbsp;&nbsp;   Trong trường hợp có cơ sở để chứng minh Khách hàng/Nhà Bán hàng cung cấp thông tin cho Sàn giao dịch không chính xác, sai lệch, không đầy đủ hoặc vi phạm pháp luật hay thuần phong mỹ tục Việt Nam thì Ban quản lý Sàn giao dịch có quyền từ chối, tạm ngừng hoặc chấm dứt quyền sử dụng dịch vụ của Khách hàng/Nhà Bán hàng.</p>
	 								<p>•  &nbsp;&nbsp;  Ban Quản Lý Sàn giao dịch giữ bản quyền sử dụng dịch vụ và các nội dung trên Sàn giao dịch theo luật bản quyền quốc tế và các quy định pháp luật về bảo hộ sở hữu trí tuệ tại Việt Nam. Nhãn hiệu Got It, hình ảnh và tất cả các biểu tượng, nội dung theo các ngôn ngữ khác nhau đều thuộc quyền sở hữu trí tuệ của Công ty Cổ Phần DAYONE. Nghiêm cấm mọi hành vi sao chép, sử dụng và phổ biến bất hợp pháp các quyền sở hữu trên.</p>
	 								<p>•  &nbsp;&nbsp;  Ban quản lý sàn giao dịch có thể chấm dứt ngay quyền sử dụng dịch vụ và quyền Khách hàng/Nhà Bán hàng của Khách hàng/Nhà Bán hàng nếu Sàn giao dịch phát hiện Khách hàng/Nhà Bán hàng đã phá sản, bị kết án hoặc đang trong thời gian thụ án, trong trường hợp Khách hàng/Nhà Bán hàng tiếp tục hoạt động có thể gây cho Sàn giao dịch trách nhiệm pháp lý, có những hoạt động lừa đảo, giả mạo, gây rối loạn thị trường, gây mất đoàn kết đối với các Khách hàng/Nhà Bán hàng khác của Sàn giao dịch, hoạt động vi phạm pháp luật hiện hành của Việt Nam. Trong trường hợp chấm dứt quyền Khách hàng/Nhà Bán hàng và quyền sử dụng dịch vụ thì tất cả các chứng nhận, các quyền của Khách hàng/Nhà Bán hàng được cấp sẽ mặc nhiên hết giá trị và bị chấm dứt.</p>
	 								<p>•  &nbsp;&nbsp;  Ban quản lý sàn giao dịch giữ bản quyền sử dụng dịch vụ và các nội dung trên Sàn giao dịch theo các quy dịnh pháp luật về bảo hộ sở hữu trí tuệ tại Việt Nam. Tất cả các biểu tượng, nội dung theo các ngôn ngữ khác nhau đều thuộc quyền sở hữu của Sàn giao dịch. Nghiêm cấm mọi hành vi sao chép, sử dụng và phổ biến bất hợp pháp các quyền sở hữu trên.</p>
	 								<p>•  &nbsp;&nbsp;  Ban quản lý sàn giao dịch giữ quyền được thay đổi bảng, biểu giá dịch vụ và phương thức thanh toán trong thời gian cung cấp dịch vụ cho Khách hàng/Nhà Bán hàng theo nhu cầu và điều kiện khả năng của Sàn giao dịch và sẽ báo trước cho Khách hàng/Nhà Bán hàng thời hạn là một (01) tháng.</p>
	 							</li>
	 							<li><b>Nghĩa vụ của Ban quản lý Sàn giao dịch</b>
	 								<p>• &nbsp;&nbsp;   Sàn giao dịch sẽ giới thiệu các sản phẩm, dịch vụ, chương trình bán hàng và các chương trình khuyến mại (nếu có) của các Nhà Bán Hàng/Thương nhân đến các Khách hàng đăng ký tham gia sau khi đã hoàn thành các thủ tục đăng ký trực tuyến các thông tin cá nhân có liên quan và các điều kiện bắt buộc mà Sàn giao dịch nêu ra.</p>
	 								<p>•  &nbsp;&nbsp;  Ban quản lý Sàn giao dịch có trách nhiệm yêu cầu các Nhà Bán Hàng/Thương nhân phải cung cấp đầy đủ và chịu trách nhiệm pháp lý về các thông tin có liên quan đến sản phẩm dịch vụ và đảm bảo tính chính xác, trung thực của thông tin về hàng hóa, dịch vụ cung cấp trên sàn giao dịch.</p>
	 								<p>•  &nbsp;&nbsp;  Ban quản lý Sàn giao dịch sẽ thực hiện kiểm tra, giám sát thông tin của thương nhân/nhà bán hàng để đảm bảo việc cung cấp thông tin của người bán trên sàn giao dịch thương mại điện tử được thực hiện chính xác đầy đủ.</p>
	 								<p>•  &nbsp;&nbsp;  Xây dựng, nâng cấp và phát triển hoạt động Sàn Giao Dịch để phục vụ tốt hơn nhu cầu của Khách hàng/Nhà Bán hàng.</p>
	 								<p>•  &nbsp;&nbsp;  Chịu trách nhiệm xây dựng Sàn giao dịch bao gồm một số công việc chính như: nghiên cứu, thiết kế, mua sắm các thiết bị phần cứng và phần mềm, kết nối Internet, xây dựng chính sách phục vụ cho hoạt động Sàn giao dịch trong điều kiện và phạm vi cho phép.</p>
	 								<p>•  &nbsp;&nbsp;  Triển khai và hợp tác với các đối tác trong việc xây dựng hệ thống các dịch vụ, các công cụ tiện ích phục vụ cho việc giao dịch của các Khách hàng/Nhà Bán hàng tham gia và người sử dụng trên Sàn giao dịch.</p>
	 								<p>•  &nbsp;&nbsp;  Lưu giữ thông tin đăng ký của thương nhân hoặc thông tin cá nhân của các cá nhân ngay từ ngày thương nhân hoặc cá nhân đăng ký tham gia Sàn giao dịch và thường xuyên cập nhật các thông tin thay đổi, bổ sung có liên quan.</p>
	 								<p>•  &nbsp;&nbsp;  Áp dụng các biện pháp cần thiết để đảm bảo an toàn thông tin liên quan đến bí mật kinh doanh của thương nhân và thông tin cá nhân. Không được tiết lộ, chuyển nhượng, cho thuê hoặc bán các thông tin liên quan đến bí mật kinh doanh hoặc thông tin cá nhân của người tiêu dùng, Khách hàng/Nhà Bán hàng khi chưa được sự đồng ý của các bên liên quan, trừ trường hợp pháp luật có quy định khác.</p>
	 								<p>•  &nbsp;&nbsp;  Duy trì hoạt động bình thường của Sàn giao dịch và nhanh chóng khắc phục các sự cố xảy ra ảnh hưởng tới hoạt động của Sàn như: sự cố kỹ thuật về máy móc, lỗi phần mềm, hệ thống đường truyền internet, nhân sự, các biến động xã hội, thiên tai, mất điện, các quyết định của cơ quan nhà nước hay một tổ chức liên quan thứ ba. Trường hợp xảy ra các sự kiện bất khả kháng như: Thiên tai, hỏa hoạn, biến động xã hội, các quyết định của cơ quan chức năng... nằm ngoài khả năng kiểm soát thì Sàn giao dịch không phải chịu trách nhiệm liên đới.</p>
	 							</li>

	 						</ol>
	 					</li>
	 					<li><b>Quyền và nghĩa vụ Khách hàng/Nhà Bán hàng tham gia Sàn giao dịch</b>
	 						<ol>
	 							<li><b>Quyền của Khách hàng/Nhà Bán hàng tham gia Sàn giao dịch</b>
	 								<p>•  &nbsp;&nbsp;  Khách hàng sẽ được lựa chọn, mua sắm sản phẩm, dịch vụ cung ứng trên Sàn Giao Dịch.</p>
	 								<p>• &nbsp;&nbsp;   Nhà Bán hàng sẽ được cấp một tên tài khoản để sử dụng trong việc quản lý những giao dịch trên Sàn Giao Dịch.</p>
	 								<p>•  &nbsp;&nbsp;  Khách hàng và Nhà Bán hàng có quyền đóng góp ý kiến cho Sàn giao dịch trong quá trình hoạt động. Các kiến nghị được gửi trực tiếp bằng thư, fax hoặc email đến cho Ban Quản Lý Sàn giao dịch.</p>

	 							</li>
	 							<li><b>Nghĩa vụ của Khách hàng/Nhà Bán hàng tham gia Sàn giao dịch</b>
	 								<p>• &nbsp;&nbsp;   Khách hàng và Nhà Bán hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ và mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình.</p>
	 								<p>•  &nbsp;&nbsp;  Khách hàng và Nhà Bán hàng có trách nhiệm thông báo kịp thời cho Ban Quản Lý Sàn giao dịch về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của mình để hai bên cùng hợp tác xử lý.</p>
	 								<p>•  &nbsp;&nbsp;  Khách hàng và Nhà Bán hàng cam kết những thông tin cung cấp cho Sàn giao dịch và những thông tin đang tải lên Sàn giao dịch là chính xác.</p>
	 								<p>•  &nbsp;&nbsp;  Khách hàng và Nhà Bán hàng cam kết, đồng ý không sử dụng dịch vụ của Sàn giao dịch vào những mục đích bất hợp pháp, không hợp lý, lừa đảo, đe doạ, thăm dò thông tin bất hợp pháp, phá hoại, tạo ra và phát tán virus gây hư hại tới hệ thống, cấu hình, truyền tải thông tin của Sàn giao dịch hay sử dụng dịch vụ của mình vào mục đích đầu cơ, lũng đoạn thị trường tạo những đơn đặt hàng, chào hàng giả, kể cả phục vụ cho việc phán đoán nhu cầu thị trường. Trong trường hợp vi phạm thì Khách hàng và Nhà Bán hàng phải chịu trách nhiệm về các hành vi của mình trước pháp luật.</p>
	 								<p>•  &nbsp;&nbsp;  Khách hàng và Nhà Bán hàng cam kết không được thay đổi, chỉnh sửa, sao chép, truyền bá, phân phối, cung cấp và tạo những công cụ tương tự của dịch vụ do Sàn giao dịch cung cấp cho một bên thứ ba nếu không được sự đồng ý của Sàn giao dịch trong Quy định này.</p>
	 								<p>•  &nbsp;&nbsp;  Khách hàng và Nhà Bán hàng không được hành động gây mất uy tín của Sàn giao dịch dưới mọi hình thức như gây mất đoàn kết giữa các thành viên bằng cách sử dụng tên đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên truyền, phổ biến những thông tin không có lợi cho uy tín của Sàn giao dịch.</p>
	 							</li>

	 						</ol>
	 					</li>
	 					<li><b>Điều khoản áp dụng</b>
	 						<p>Quy chế của Sàn giao dịch chính thức có hiệu lực thi hành kể từ ngày ký Quyết định ban hành kèm theo Quy chế này. Sàn giao dịch có quyền và có thể thay đổi Quy chế này bằng cách thông báo cho tất cả các Khách hàng và Nhà Bán hàng, đối tượng sử dụng dịch vụ sàn giao dịch ít nhất 5 ngày trước khi áp dụng những thay đổi đó. Việc thành viên tiếp tục sử dụng dịch vụ sau khi Quy chế sửa đổi được công bố và thực thi đồng nghĩa với việc thành viên đã chấp nhận Quy chế sửa đổi này.</p>
	 						<p>Khách hàng và Nhà Bán hàng tham gia Sàn Giao Dịch có trách nhiệm tuân theo quy chế hiện hành khi giao dịch trên website.</p>
	 						<p>Địa chỉ liên lạc chính thức của Sàn giao dịch và nhận tất cả thông tin khiếu nại, báo cáo vi phạm tại:</p>
	 						<p><b>CÔNG TY CỔ PHẦN DAYONE</b></p>
	 						<p>Địa chỉ: Lầu 6, Toà nhà Khải Hoàn, 1Bis Nguyễn Văn Thủ, P.Đa Kao, Q.1, TP.HCM</p>
	 						<p>Điện thoại: 1900 55 88 20    &nbsp;&nbsp;&nbsp;&nbsp;         Fax: (08) 3910 0080   </p>
	 						<p>Email: support@gotit.vn  </p>

	 					</li>
	 				</ol>

	 			</div>
	 		</div>
	 	</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		var w_width = $(window).width();
	    var w_height = $(window).height();
	    var footer_height = $('footer').height();
	    $('.accordion-section').css({'min-height':(w_height - footer_height - 63)+'px'});

	    $(window).resize(function(){
            var w_width = $(window).width();
            var w_height = $(window).height();
            var footer_height = $('footer').height();
            $('.accordion-section').css({'min-height':(w_height - footer_height - 63)+'px'});
        })
	})
</script>
@endsection
