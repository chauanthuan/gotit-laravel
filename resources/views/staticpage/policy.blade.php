@extends('layouts.master')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/policy.css') !!}">
@endsection
@section('content')
<section class="section-content">
	<div class="container" id="static_page">
		<div class="row row-no-padding">
			<div class="policy_page" style="padding: 10px;margin: 40px 120px 0px 120px;">
				<strong class="title">Privacy Policy</strong>
				<div>
					<p><strong>Assurance of transaction safety</strong></p>
					<p>Got It is committed to safeguarding your privacy online by using appropriate measures on technical security to protect your personal infoamrtion and your payment information as follows:</p>
					<ul>
						<li>We use Secure Sockets Layer encryption (SSL) to protect customers’ information to protect your information during data transmission by encrypting all information input by customer.</li>
						<li>When you access Got It, we use Cookies which are text files placed on your computer to help the website analyze how users have access to the web. Cookies are stored by internet browser on your computer's hard drive. We use cookies for your convenience on the web (for example, remembering your username when you want to change the shopping cart without having to re-enter your email address) and does not require any information about you (for example, targeted advertising). Those cookies shall be encrypted for security. Your browser can be set not to use cookies, but this will restrict your website access. Please accept our commitment that cookie does not include any personal information and security with virus.
						</li>
						<li>With payment option of payment gateway, we use Sacombank e-Pay payment gateway. With Sacombank e-Pay, information of card and cardholder shall be secured by the most advanced security technology, including:
							<br><br>
						</li>
						<li>With payment option of e-wallet, we use MoMo e-wallet of M_Service. Momo e-wallet has security infrastructure of 256 bits, similar to those used by banks and verified by Verisign to assure transaction security.
						</li>
						<li>We only provide the last 4 digit of your card when we confirm the order. We will, certainly, provide full card number to your card issuing company to process your order.</li>
					</ul>
					<p>To ensure successful transation and minimize all risks realated, you should acknowledge and comply with the following commitments:</p>
					<ul>
						<li>We recommend that you should not give detailed information about payments to anyone by e-mail, we do not take responsibility for the losses you may incur in the exchange of information on your Internet or email.</li>
						<li>You absolutely do not use any programs, tools or any other forms to interfere in the system or alter data structures. It is strictly forbidden to distribute, propagate or encourage any activity aimed at interventing, sabotaging or infiltrating the website system data. All violations will be stripped of all rights and will be prosecuted under the law if necessary.
						</li>
						<li>All information are confidential, unless require to be disclose by law.</li>
					</ul>

				</div>
				<div>
					<p><strong>Privacy protection</strong></p>
					<p><b><u>Collect personal information</u></b></p>
					<ul>
						<li>Some personal information is gathered when you register. During registration, we ask for your email address, phone number, ID and passwords, and your street address. Those are information required by Got It in order to contact customer when customer uses services provided by Got It to ensure consumer rights.
						</li>
						<li>Got It will collect other information when you input on Got It or send to Got It. We collect, store and process your information for the purpose of customer service and feedback, providing suitable services, enhancing products/services quality, and contacting customer when needed.
						</li>
						<li>We will collect other information when you want to place an order on the website, including: order history, transaction value, gift sending channels, payment options for future problem solving. </li>
						<li>You must be committed to protecting personal data, including ID, password, and email. We do not take any responsibility for the misuse of password if it is not our fault. We recommend that you do not divulge your password to anyone. Got It will never ask you for your password in an unsolicited phone call or in an unsolicited email. As an extra security precaution, you may want to logoff once you have completed a session on Got It. This is to ensure that others cannot access your personal information and correspondence if you share a computer with someone else or are using a computer in a public place like a library or Internet cafe. You should notify Got It on any violation, abusement, illegal use of your ID and password for appropriate resolution.
						</li>
					</ul>
					<p><b><u>How your information is used</u></b></p>
					<ul>
						<li>To provide services to customers</li>
						<li>To exchange information between customers and Got It</li>
						<li>To protect user account or to prevemajocounterfeit</li>
						<li>To contact customer for complaint handling</li>
						<li>To enhance customer service quality</li>
						<li>Not to use your personal information for reasons other than order confirmation, transaction-related issues, and informing Got It service.</li>
						<li>We do not disclose your personal information. All information are confidential, unless require to be disclose by law.</li>
					</ul>

					<p><b><u>How long your personal information will be stored</u></b></p>
					<p>Your personal information shall be stored until termation request by customer or directly deletion by customer, otherwise, all personal information shall be securely stored on Got It’s server.</p>

					<p><b><u>How you can access, update or delete your information</u></b></p>
					<ul>
						<li>We will provide you with the means to ensure that your personal information is correct and current. You may edit or delete your user profile at any time by logging onto your account.</li>
						<li>User should notify Got It on any disclosure of personal data to third party. Got It will confirm the information received and shall be responsible to provide reason and provide guidance on restoring and securing account. Email for customer service is <a href="mailto:support@gotit.vn">support@gotit.vn</a>
						</li>
					</ul>

					<p><b><u>Security</u></b></p>
					<ul>
						<li>We use Secure Sockets Layer encryption (SSL) to protect customers’ information to protect your information during data transmission by encrypting all information input by customer.</li>
						<li>You must be committed to protecting personal data, including ID, password, and email. We do not take any responsibility for the misuse of password if it is not our fault. We recommend that you do not divulge your password to anyone. Got It will never ask you for your password in an unsolicited phone call or in an unsolicited email. As an extra security precaution, you may want to logoff once you have completed a session on Got It. This is to ensure that others cannot access your personal information and correspondence if you share a computer with someone else or are using a computer in a public place like a library or Internet.
						</li>
						<li>gotit.vn commits not to dislose your personal information for commercial use that violates the agreement between Got It and customers. </li>
						<li>Privacy policy of Got It shall only be applied at www.gotit.vn. This policy does not include or relate to third parties who place advertisement or partner with Got It at gotit.vn. You should refer and differentiate differences in privacy policy of those third parties’ websites. 
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		var w_width = $(window).width();
	    var w_height = $(window).height();
	    var footer_height = $('footer').height();
	    $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});

	    $(window).resize(function(){
            var w_width = $(window).width();
            var w_height = $(window).height();
            var footer_height = $('footer').height();
            $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});
        })
	})
</script>
@endsection