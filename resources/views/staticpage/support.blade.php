@extends('layouts.master')
@section('styles')
<link rel="stylesheet" href="{!! asset('css/support.css') !!}">
@endsection
@section('content')
<section class="accordion-section faq_page section-content">
   <div class="container-fluid">
      <div class="row head">
         <div class="col-md-12">
            <div class="box_top tabbable-line">
               <h2 class="">Frequently asked questions</h2>
               <div class="dropdown-mobile wrapper-dropdown" id="tab_all_faq" tabindex="1">
                  <span>Sort by<small>For receiver</small></span>
                  <ul class="nav nav-tabs ">
                     <li class="active">
                        <a aria-expanded="true" data-toggle="tab" href="#tab_accord_1">
                        For receiver</a>
                     </li>
                     <li class="">
                        <a aria-expanded="false" data-toggle="tab" href="#tab_accord_2">
                        For sender </a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="box_content">
         <div class="row">
            <div class="tab-content">
               <div class="tab-pane active" id="tab_accord_1">
                  <div class="panel-group" id="accordion">
                     <!-- start panel left -->
                     <div class="panel-left col-sm-6">
                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_1">
                                    How can I use a Got It eGift (mobile gift)?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_1" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <p>Please show your eGift code to the cashier upon redemption to redeem the product/service.</p>
                                    <p>For more information on how to use a Got It eGift, please read the 'What is this' section for more information.</p>
                                 </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_2">
                                    What if I lost the eGift?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Your Got It eGift is valuable as it can be exchanged for legitimate products/services. Please keep it safe and secure and avoid sharing the eGift code with anyone.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_3">
                                    What if I want to chose a product/service other than what is on the eGift ?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Please accept the gift you've been sent. In case of the product/service on eGift is temporarily unavailable at that time or place, please contact the staff member for advice on selecting the equivalent value product/service or refer to the other branch store accept the exchange.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_4">
                                    What if I spend more or less than the nomination on a Got It eGift?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Got It eGift is only using once. Please use the all value of eGift.</p>
                                    <p>Store staff will not repay you if you haven't used all the value of eGift and has the right to ask you to pay the excess money of the value of eGift.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div> 
                    <!-- end panel left -->


                    <!-- start panel right -->
                    <div class="panel-left col-sm-6">
                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_7">
                                    How do I know which stores accept Got It eGifts?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_7" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Please click on the “Store list” section of the eGift to view the stores that accept Got It eGifts.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_8">
                                    Can I give my eGift to others to redeem for me?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Yes you can, however, please be aware that each voucher can only be redeemed once. We recommend you keep your eGift secure and avoid sharing the code of your product with anyone.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#TEST_9">
                                    What if my eGift is expired?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_9" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Please call our hotline on: 1900 55 88 20 (from 8am – 6pm) or email us at support@gotit.vn.
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div> 
                    <!-- end panel right -->
                  </div>
               </div>
               <div class="tab-pane" id="tab_accord_2">
                  <div class="panel-group" id="accordion2">
                     <!-- start panel left -->
                     <div class="panel-left col-sm-6">
                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#TEST_12">
                                    Do I need to sign up for an account to send a gift?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_12" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <p>Yes. You do need an account to purchase gifts and check your orders after purchasing. Besides that, with an account from Got It, you can choose to receive newsletters from us. You can sign up on website of Got It with your Facebook or Google account.</p>
                                 </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#TEST_22">
                                    How do I sign up?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_22" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>You can sign up via Facebook or Google account.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#TEST_23">
                                    Is my personal information and credit card details secure?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_23" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Yes. Got It uses a high-end technical software system to ensure the security of your personal information and your purchasing.</p>
                                    <p>Please read: https://beta.gotit.vn/privacypolicy for more information on our privacy policy.</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div> 
                    <!-- end panel left -->


                    <!-- start panel right -->
                    <div class="panel-left col-sm-6">
                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#TEST_27">
                                    What if I have issues purchasing a gift?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_27" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If you have any issues with payment, Got It is willing to support you.</p>
                                    <p>Please call our hotline on 1900 55 88 20 (from 8am to 11pm) for direct support</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#TEST_28">
                                    How can I check the gifts that I've sent?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_28" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>In order to check your order, please choose “Gift sent” below your account icon or click on this link: https://beta.gotit.vn/gift-sent</p>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->

                        <!-- start panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                 <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#TEST_29">
                                    How can I send multiple gifts to multiple people at once?
                                 </a>
                                </h4>
                            </div>
                            <div id="TEST_29" class="panel-collapse collapse">
                                <div class="panel-body">
                                    PPlease call our hotline on 1900 55 88 20 (from 8am to 11pm) for direct support.
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div> 
                    <!-- end panel right -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('scripts')
<script>
   $(document).ready(function(){
      $('#accordion').on('show.bs.collapse', function () {
         $('#accordion .in').collapse('hide');
      });
      $('#accordion2').on('show.bs.collapse', function () {
         $('#accordion2 .in').collapse('hide');
      });

      var w_width = $(window).width();
      var w_height = $(window).height();
      var footer_height = $('footer').height();
      $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});


      $(window).resize(function(){
          var w_width = $(window).width();
          var w_height = $(window).height();
          var footer_height = $('footer').height();
          $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});
       })
   })
</script>
@endsection