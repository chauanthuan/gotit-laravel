@extends('layouts.master')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/terms.css') !!}">
@endsection
@section('content')
<section class="section-content">
    <div class="container" id="static_page">
        <div class="row row-no-padding">
            <div class="teamof_page" style="padding: 10px;margin: 40px 120px 0px 120px;">
                <strong class="title">Terms of Use</strong>
                <div>
                    <p><b>Acceptance of Terms &amp; Conditions</b></p>
                    <p>
                    Please read the Terms &amp; Conditions carefully before accessing and using gotit.vn Website (hereinafter called the "Got It") of DAYONE Joint Stock Company (hereinafter called the "DAYONE"). By accepting the Terms &amp; Conditions, you agree to abide by the rules governing the use of services offered by the Got It. If you have any questions about this agreement, please contact us via e-mail at <a href="mailto:support@gotit.vn.">support@gotit.vn.</a></p>
                    <p><strong>Got It Service</strong></p>
                    <p>
                    Got It is a platform that provides a premium instant mobile gifting service by offering Products/Services of legal merchants who have Partnership Agreement with DAYONE. Got It is a one-stop gift shop that is designed to allow customers to select, purchase, and send a Mobile Gift Coupon to receivers via such platform as SMS, email, or over-the-top messenger (OTT), or social networks. The receivers shall then use the Mobile Gift Coupon to exchange for the Product/Service at the agreed redemption stores.</p>
                    <p>
                    To have the full use of services offered on Got It, you need to sign up to create an account and provide Got It with certain personal information including but not limited to your email address for further communications between you and Got It. By signing up, you agree to receive our newsletters, emails and other communications about our products or services. Whenever you wish not to receive those communications anymore, you can notify us by checking the unsubscribing function available on at the bottom of our emails. Any personal information that you submit shall be taken care of in compliance with the Privacy Policy of Got It. Each user must take responsibility for their password, account and activities on the website. You understand and agree that you are responsible for all personal information relating to the use of your account, and are fully responsible for allowing anyone to access to your account.</p>
                    <p>
                    You might order the Products/Services at the quotations and standards by legal merchants that are made available on Got It and your orders shall be subject to the Got It’s acceptance in full or in part of your orders. Got It’s acceptance shall be in effect after your payment has been made or identified.</p>
                    <p><strong>Terms of purchase, payment, and delivery</strong></p>
                    <p><b><u>Purchase and Payment</u></b></p>
                    <p>
                        You might order the Products/Services on Got It, you shall proceed with the following steps:
                    </p>
                    <ul>
                        <li>- Step 1: You select Products/Services listed on Got It that you are looking for.</li>
                        <li>- Step 2: Based on available information of Products/Services, you might make the decision on proceeding with online order via clicking “Send gift” or “Add gift to Cart”. If you have not signed in onto your Got It account, you shall be requested to sign in and proceed with online payment.</li>
                        <li>- Step 3: You select the quantity of purchased Products/Services.</li>
                        <li>- Step 4: You input your personal message for the gift receiver.</li>
                        <li>- Step 5: You select the preferred online payment option; including debit, credit card payment with global brands like Visa, Master, JCB via Sacombank e-Pay payment gateway MoMo e-wallet payment. You then proceed with online payment by providing authentification of your payment.</li>
                        <li>- Step 6: You select the platform that the receiver will receive your mobile gift coupon, including SMS, Email, Over-the-top messengers, and Social networks.</li>
                        <li>- Step 7: Receiver shall then redeem mobile gift coupon at locations specified on mobile gift coupon or listed on Got It for real Products/Services under the terms of use and before the expiry date that are specified on mobile gift coupon. </li>
                    </ul>
                    
                    <p><b><u>Delivery:</u></b></p>
                    <p>
                        Once you make online payment, you will select the platform which the receiver will receive your mobile gift coupon, including SMS, Email, Over-the-top messengers, and Social networks. Receiver shall then redeem mobile gift coupon at locations specified on mobile gift coupon or listed on Got It for real Products/Services. Therefore, Got It will not physically deliver the Products/Services and gift coupon to customers, Got It only electronically delivers mobile gift coupon to customers’ electronic platforms.
                    </p>
                    <p><b><u>Return of Products/Service:</u></b></p>
                    <p>
                    After you have completed your payment for Products/Services, we will not accept any requests to terminate or cancel the purchased Products/Services, or refund the transaction value or change any Products/Services.</p>
                    <p>
                    After you redeem mobile gift coupon for real Products/Services at merchant locations, you may return Products/Service in accordance with the terms and conditions of the merchant who provides that Product/Service. </p>
                    <p>
                    Mobile gift coupon of Got It issued to customers cannot be exchanged for cash.</p>
                    <p><b><u>Warranty of Products/Services:</u></b></p>
                    <p>Terms and conditions of the warranty of Products/Service shall be in accordance with the terms and conditions of the merchant who provides that Product/Service.</p>
                    <p><strong>Rights and Responsibilities</strong></p>
                    <p><b><u>Rights and Responsibility of Got It</u></b></p>
                    <ol>
                        <li>
                            <p><b>Rights of Got It</b></p>
                            <ul>
                                <li>Issue and execute the Terms of Use for users of Got It</li>
                                <li>In case there is proof that customers/merchants provide incorrect, false, law-violating, custom-violating information, Got It has the right to refuse, suspend, or withdraw the user right to use Got It</li>
                                <li>In case users violates Term of Use of Got It or carries out any act that might impact on Got It operation, Got It has the right to withdraw users from one service or all services provided by Got It and shall inform users at least three (03) days in advance.</li>
                                <li>In case users suffer from bankruptcy, or found convicted, or might cause Got It to bear any legal responsibility, or conduct any fraudulent act, or vilotes any law or regulation, Got It has the right to withdraw user right to use Got It immediately. In these termination cases, all records, credentials, and rights of users shall be deemed to expire.
                                </li>
                                <li>Got It reserves all intellectual properties of content on Got It under the international and Vietnam’s intellectual laws. You may not modify, copy, reproduce, transmit, distribute, publish, create derivative works from, display or otherwise transfer or commercially exploit any of the content, in whole or in part.
                                </li>
                                <li>Got It reserves the right to change Products/Services, prices, price calculation methods, and other changes of Got It during each time period without any advance notice if deem appropriate.</li>
                            </ul>
                        </li>
                        <li>
                            <p><b>Responsibility of Got It</b></p>
                            <ul>
                                <li>Introduce Products/Services and special offers from merchants to customers after customers register personal information with Got It and accept Got It’s terms of use.</li>
                                <li>Request the provision of legal documents of Products/Services information to merchants to ensure that such information is accurate and reliable, and legitimate.</li>
                                <li>Build, enhance, and develop Got It to better serve customers/merchants, including but not limiting to research, design, purchase hardwares and softwares, internet connection, and construct website policy.</li>
                                <li>Cooperate with partners in deploying tools to facilitate the transaction of users of Got It.</li>
                                <li>Store personal information of users of Got It and update any related changes.</li>
                                <li>Apply privacy policy for business secrecy of merchant and personal information of customers. We do not disclose your personal information. All information are confidential, unless require to be disclose by law.</li>
                                <li>Maintain normal operation of Got It and promptly repair any incidences that affect the normal operation of Got It, such as hardware technical fault, software error, internet connection error, force majeure events, power disruption, changes in Governement policy and regulation. In any event of force majeure that is not under the control of Got It, Got It shall not hold any related responsibility.</li>
                            </ul>
                        </li>
                    </ol>

                    <p><b><u>Rights and responsibility of Customers/Merchants</u></b></p>
                    <ol start="3">
                        <li>
                            <p><b>Rights of Customers/Merchants</b></p>
                            <ul>
                                <li>Customer can select and purchase Products/Services provided on Got It, after customers register personal information with Got It and accept Got It’s terms of use, and provide with an account.</li>
                                <li>Merchant will be provided an account for transaction-related management.</li>
                                <li>Customers/merchants have the right to send any feedback to Got It. All feedback should be sent in writing, or by fax, email to Got It.</li>
                            </ul>
                        </li>
                        <li>
                            <p><b>Responsibilites of Customers/Merchants</b></p>
                            <ul>
                                <li>Must be committed to protecting personal data, including ID, password, and email.</li>
                                <li>Should notify Got It on any violation, abusement, illegal use of your ID and password for appropriate resolution.</li>
                                <li>Must be committed that information provided to Got It is legitimate and accurate.</li>
                                <li>Must be committed not to conduct any fraudulent act, or vilotes any law or regulation, threatening, improper probe of information, use of any programs, tools or any other forms to interfere in the system or alter data structures, or to distribute, propagate or encourage any activity aimed at interventing, sabotaging or infiltrating the website system, or use Got It for speculative purpose by making fake orders. All violations will be stripped of all rights and will be prosecuted under the law if necessary.
                                </li>
                                <li>You may not modify, copy, reproduce, transmit, distribute, publish, create derivative works from, display or otherwise transfer or commercially exploit any of the content, in whole or in part.</li>
                                <li>You may not discredit Got It’s reputation in cases such as confusing other members by using second registered ID, directly or indirectly, via a third party, misinform Got It.</li>
                            </ul>
                        </li>
                    </ol>

                    <p><b>Intellectual Property Rights</b></p>
                    <p>You acknowledge that Got It permits access to content that is protected by copyrights, trademarks, and other proprietary (including intellectual property) rights, and that these Intellectual Property Rights are valid and protected in all media existing now or later developed and except as is explicitly provided below, customer’s use of content shall be governed by applicable copyright and other intellectual property laws.</p>
                    <p>
                    You agree and acknowledge that all the content on Got It shall be solely under the Intellectual Property Rights of VietnamWorks DAYONE and you may not modify, copy, reproduce, transmit, distribute, publish, create derivative works from, display or otherwise transfer or commercially exploit any of the Content, in whole or in part.</p>
                    <p>
                    Any permitted copies of the content must reproduce any notices contained in the content, such as all Intellectual Property Right notices, and an original source attribution to Got It and its URL address. You do not acquire any Intellectual Property Rights by downloading or printing content. Any unauthorized copies or uses of the Content for commercial purposes shall be deemed as a breach and shall be treated in accordance with the relevant applicable laws and provisions herein.</p>
                    <p><b>Terms of Use</b></p>
                    <p>
                    Got It reserves the right to amend, adjust this Terms of Use by advance notice to users and merchants at least five (05) days before those amendments take effective. When users continue to use Got It after amendments of Terms of Use means that users accept those changes.</p>
                    <p>
                    Users and merchants have the responsibility to comply with current Terms of Use on Got It.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var w_width = $(window).width();
        var w_height = $(window).height();
        var footer_height = $('footer').height();
        $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});

        $(window).resize(function(){
            var w_width = $(window).width();
            var w_height = $(window).height();
            var footer_height = $('footer').height();
            $('.section-content').css({'min-height':(w_height - footer_height - 63)+'px'});
        })
    })
</script>
@endsection