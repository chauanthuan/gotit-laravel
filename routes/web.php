<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});
Route::get('/about', function () {
    return view('staticpage.about_gotit');
});
Route::get('/about-egift', function () {
    return view('staticpage.about_egift');
});
Route::get('/terms', function () {
    return view('staticpage.termofuse');
});
Route::get('/privacypolicy', function () {
    return view('staticpage.policy');
});
Route::get('/operatingregulation', function () {
    return view('staticpage.operate');
});
Route::get('/support', function () {
    return view('staticpage.support');
});

Route::get('/account', function () {
    return view('account.account');
})->name('account.account');
Route::get('/changepassword', function () {
    return view('account.changepassword');
});
Route::get('/gift-sent', function () {
    return view('gift-sent.gift-sent');
});
Route::get('/gift-received', function () {
    return view('gift-received.gift-received');
});
Route::get('/send-egift', 'ProductController@detail');

Route::get('/signup', function () {
    return view('guest.signup');
});
Route::get('/forgotpassword', function () {
    return view('guest.forgotpassword');
});
Route::get('/login', function () {
    return view('guest.login');
});
Route::get('/checkout', function () {
    return view('checkout.checkout');
});